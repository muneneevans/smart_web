<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "main";
$route['404_override'] = 'main/pagenotfound';


//routes for the website pages: 
//Allow flexibility in the URL of the website
$route['home'] = "main/home" ;
$route['services'] = "main/services" ;
$route['products'] = "main/products" ;
$route['solutions'] = "main/products" ;
$route['about'] = "main/about" ;
$route['clients'] = "main/clients" ;
$route['customers'] = "main/clients" ;
$route['partners'] = "main/clients" ;
$route['news'] = "main/news" ;
$route['media'] = "main/news" ;
$route['manuals'] = "main/manuals" ;
$route['howsmartworks'] = 'main/howsmartworks' ;
$route['faq'] = "main/faqs" ; 
$route['faqs'] = "main/faqs" ; 
$route['contact'] = "main/contact" ;
$route['about'] = "main/about" ;
$route['careers'] = "main/careers" ;


//routes for the login pages
$route['login'] = "forms/login" ;

//route for the admin
$route['dashboard'] = "admin/dashboard" ;










/* End of file routes.php */
/* Location: ./application/config/routes.php */