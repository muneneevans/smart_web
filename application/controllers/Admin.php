<?php
class Admin extends CI_Controller {

        private $data ; 
        
        public function __construct()
        {
                parent::__construct();
                $this->load->model('app_model');
                $this->load->helper('url_helper');
                $this->load->library('form_validation') ;
                $this->form_validation->set_error_delimiters('<div class="errors ">', '</div>');
                $this->load->helper(array('form', 'url'));
                $this->load->library('session')  ;
               date_default_timezone_set('Africa/Nairobi');
        }

        private function CheckLogin()
        {
                if(!$this->session->has_userdata('user_name'))
                {
                        redirect('index.php/forms/login') ;
                }
        }

        public function load_view()
        {
                $this->load->view('templates/header',$this->data );
                $this->load->view('templates/sidebar',$this->data );
                $this->load->view('admin/'.$this->data['page']) ;                
               // $this->load->view('templates/footer');
        }  

        public  function dashboard()
        {
                $this->CheckLogin();
                $this->data['messages'] = $this->app_model->GetUnreadMessages();
                $this->data['title'] = 'Dashboard' ; 
                $this->data['page'] = 'dashboard' ; 
                $this->load_view();

        }


        public function index()
        {
                $this->CheckLogin();      
                redirect('index.php/admin/dashboard') ;              
        }


        //creating a new news story
        public function newstory()
        {
                 $this->CheckLogin();
                //set configuration for image uploads
                $config['upload_path'] = 'assets/img/news/'   ;
                $config['allowed_types'] = '*' ;
                $config['max_size'] = 10000; 
                $config['max_width'] = 1920 ; 
                $config['max_height'] = 1200 ;
                

                //set configuration for fields
                $validation_config = array(
                        array(
                                'field' => 'title',
                                'label' => 'Title',
                                'rules' => 'required',
                                'errors'=> array(
                                        'required' => 'The News title cannot be left empty'
                                ),
                        ),
                        array(
                                'field' => 'short_form' , 
                                'label' => 'title' , 
                                'rules' => 'required' , 
                                'errors'=> array(
                                        'required' => 'The short form cannot be left empty'
                                ),
                        ),
                        array(
                                'field' => 'content' , 
                                'label' => 'Content' ,
                                'rules' => 'required' ,
                                'errors' => array(
                                        'required' => 'The news content cannot be left empty'
                                ),
                        ),
                        array(
                                'field' => 'banner_image' ,
                                'label' => 'Image' , 
                                'rules' => 'callback_validateimage',
                        ),
                );


                $this->form_validation->set_rules($validation_config) ; 
                $this->load->library('upload') ; 
                $this->upload->initialize($config);
            
                
                if (  $this->form_validation->run() == False)
                {       
                       
                       
                        $this->data['title'] = 'New Story'    ;        
                        $this->data['page'] = 'newstory' ;
                        $this->load_view();
                }
                else
                {           
                                     
                        if( ! $this->upload->do_upload('banner_image'))
                        {

                                $this->data['error'] = $this->upload->display_errors() ;
                                $this->data['title'] = 'new story' ; 
                                $this->data['page']  = 'newstory' ; 
                                $this->load_view();

                        }
                        else
                        {
                                $data['upload_data'] = $this->upload->data();


                                $data = array(
                                        'title' => $this->input->post('title'),    
                                        'short_form' =>  $this->input->post('short_form'),
                                        'content' => $this->input->post('content') ,
                                        'banner_image' => $this->upload->data('file_name') ,
                                        );  

                                if( $this->app_model->AddNewStory($data))
                                {
                                        redirect('index.php/admin/dashboard');                                        
                                }
                                else
                                {                                        
                                        $this->data['title'] = 'new story' ; 
                                        $this->data['page']  = 'newstory' ; 
                                        $this->load_view();
                                }
                        }       
                }
               
                 
        }

        //validate file upload as picture only
        public function validateimage()
        {
                 $this->CheckLogin();
                if($this->input->post('replace_image') == False)
                {
                     return true ;   
                }
                
                $ext =  $_FILES['banner_image']['type'] ;
                $Allowed = FALSE ;
                switch($ext)
                {
                        case "image/png" :  
                        $Allowed = TRUE ;
                        break;

                        case "image/x-png" :  
                        $Allowed = TRUE ;
                        break;

                        case "image/jpeg" :  
                        $Allowed = TRUE ;
                        break;

                        case "image/pjpeg" :  
                        $Allowed = TRUE ;
                        break;

                        case "image/bmp" :  
                        $Allowed = TRUE ;
                        break;

                        case "image/x-windows-bmp" :  
                        $Allowed = TRUE ;
                        break;

                        case "image/gif" :  
                        $Allowed = TRUE ;
                        break;                        
                        
                }
                
                if( ! $Allowed)
                {
                        $this->form_validation->set_message('validateimage', "The file selected is not a valid image");
                        return FALSE ; 
                }
                else
                {
                        return TRUE ; 
                }
        }

        //get all messages 
        public function messages()
        {
                $this->CheckLogin();
                $this->data['messages'] = $this->app_model->GetAllMessages();
                $this->data['title'] = 'Dashboard' ; 
                $this->data['page'] = 'dashboard' ; 
                $this->load_view();
        }

        public function allnews()
        {
                $this->CheckLogin();
                $this->data['news'] = $this->app_model->GetAllNews();
                $this->data['title'] = 'News: Admin' ; 
                $this->data['page'] = 'stories' ; 
                $this->load_view();
        }

        public function editstory($news_id)
        {
                $this->CheckLogin();
                $data = array(
                        'id' => $news_id 
                );
                $result =  $this->app_model->GetStorySecure($data)  ;
                if( $result == FALSE)
                {
                        echo "no record found" ;
                }
                else
                {
                        $story = $result[0];
                        
                        $_POST['story_title'] = $story['title'];
                        $_POST['short_form'] = $story['short_form'] ;
                        $_POST['content'] = $story['content'] ; 
                        $_POST['banner_image'] = $story['banner_image'] ;
                        $_POST['news_id'] = $story['news_id'] ;
                        
                        $this->data['banner_image'] = $story['banner_image'] ;
                        $this->data['title'] = "Edit story: Admin" ; 
                        $this->data['page'] = 'editstory' ; 
                        $this->load_view();
                }
        }

        public function updatestory()
        {
                $this->CheckLogin();
                //set configuration for image uploads
                $config['upload_path'] = 'assets/img/news/'   ;
                $config['allowed_types'] = '*' ;
                $config['max_size'] = 10000; 
                $config['max_width'] = 1920 ; 
                $config['max_height'] = 1200 ;
                $config['overwrite'] = TRUE ;

                //set configuration for fields
                $validation_config = array(
                        array(
                                'field' => 'title',
                                'label' => 'Title',
                                'rules' => 'required',
                                'errors'=> array(
                                        'required' => 'The News title cannot be left empty'
                                ),
                        ),
                        array(
                                'field' => 'short_form' , 
                                'label' => 'title' , 
                                'rules' => 'required' , 
                                'errors'=> array(
                                        'required' => 'The short form cannot be left empty'
                                ),
                        ),
                        array(
                                'field' => 'content' , 
                                'label' => 'Content' ,
                                'rules' => 'required' ,
                                'errors' => array(
                                        'required' => 'The news content cannot be left empty'
                                ),
                        ),
                        array(
                                'field' => 'banner_image' ,
                                'label' => 'Image' , 
                                'rules' => 'callback_validateimage',
                        ),
                );


                $this->form_validation->set_rules($validation_config) ; 
                $this->load->library('upload') ; 
                $this->upload->initialize($config);
            
                if (  $this->form_validation->run() == False)
                {       
                        $this->data['title'] = 'Update story'    ;        
                        $this->data['page'] = 'editstory' ;
                        $this->load_view();
                }
                else
                {
                        if($this->input->post('replace_image') == True)
                        {
                                if( ! $this->upload->do_upload('banner_image'))
                                {
                                        
                                        $this->data['error'] = $this->upload->display_errors() ;
                                        $this->data['title'] = 'new story' ; 
                                        $this->data['page']  = 'editstory' ; 
                                        $this->load_view();

                                }
                                else
                                {

                                        $data['upload_data'] = $this->upload->data();


                                        $data = array(
                                                'news_id'=> $this->input->post('news_id') ,
                                                'title' => $this->input->post('title'),    
                                                'short_form' =>  $this->input->post('short_form'),
                                                'content' => $this->input->post('content') ,
                                                'banner_image' => $this->upload->data('file_name')   ,
                                                );  

                                        if( $this->app_model->UpdateStory($data))
                                        {
                                                redirect('index.php/admin/allnews') ;                                       
                                        }
                                        else
                                        {
                                               $this->data['error'] = $this->upload->display_errors() ;
                                                $this->data['title'] = 'new story' ; 
                                                $this->data['page']  = 'editstory' ; 
                                                $this->load_view();
                                        }
                                }       
                        }
                        else
                        {
                                
                                $data = array(
                                                'news_id'=> $this->input->post('news_id') ,
                                                'title' => $this->input->post('title'),    
                                                'short_form' =>  $this->input->post('short_form'),
                                                'content' => $this->input->post('content') ,                                                
                                                );  

                                        if( $this->app_model->UpdateStory($data))
                                        {
                                                redirect('index.php/admin/allnews') ;                                        
                                        }
                                        else
                                        {
                                                echo 'without failed' ;
                                        } 
                        }
                }
               
                 
        }

        public function logout()
        {
                $this->session->unset_userdata('user_name');
                $this->session->sess_destroy();
                redirect('index.php/forms/login');
        }

        public function newjob()
        {
                 $this->CheckLogin();
                  $validation_config = array(
                        array(
                                'field' => 'title',
                                'label' => 'Title',
                                'rules' => 'required',
                                'errors'=> array(
                                        'required' => 'The News title cannot be left empty'
                                ),
                        ),
                        array(
                                'field' => 'description' , 
                                'label' => 'description' , 
                                'rules' => 'required' , 
                                'errors'=> array(
                                        'required' => 'The short form cannot be left empty'
                                ),
                        ),
                        array(
                                'field' => 'skills' , 
                                'label' => 'skills' ,
                                'rules' => 'required' ,
                                'errors' => array(
                                        'required' => 'The news content cannot be left empty'
                                ),
                        ),
                        
                );

                 $this->form_validation->set_rules($validation_config) ;
                if (  $this->form_validation->run() == False)
                {  
                        $this->data['title'] = "New Job" ; 
                        $this->data['page'] = "newjob" ; 
                        $this->load_view() ; 
                }
                else
                {
                        $data = array(
                                'title' => $this->input->post('title'),
                                'description' => $this->input->post('description'),
                                'skills' => $this->input->post('skills'),
                        );
                        if($this->app_model->AddNewJob($data))
                        {
                                redirect('index.php/admin/dashboard');
                        }
                        else
                        {
                                echo 'failed' ; 
                        }
                } 
        }

        public function newvacancy()
        {
             $this->CheckLogin();
             $this->data['emptyjobs'] = $this->app_model->GetEmptyVacancies();

             $validation_config = array(
                     array(
                             'field' => 'job_id' , 
                             'label' => 'Job_id' , 
                             'rules'  => 'required' , 
                             'errors' => array(
                                     'required' => 'please select a job'
                             ),

                     ),
                     array(
                             'field' => 'people_needed' , 
                             'label' => 'People Needed' , 
                             'rules'  => 'required|numeric|greater_than_equal_to[1]' , 
                             'errors' => array(
                                     'required' => 'people needed cannot be empty' ,
                                     'numeric' => 'please enter a number',
                                     'greater_than_equal_to' => 'the number cannot be negative',
                             ),
                        ),
                     array(
                             'field' => 'country' , 
                             'label' => 'Country' , 
                             'rules'  => 'required' , 
                             'errors' => array(
                                     'required' => 'please enter a country'
                             ),
                     ),
                     array(
                             'field' => 'deadline' , 
                             'label' => 'deadline' , 
                             'rules' => 'required|callback_date_valid' ,
                             'errors' => array(
                                     'required' => 'please enter a date',                                     
                             ),
                     ),
             );   

             $this->form_validation->set_rules($validation_config) ;
             if($this->form_validation->run() == FALSE)
             {
                     $this->data['jobs'] = $this->app_model->GetAlljobs();
                     $this->data['title'] = 'New Vacancy'    ;        
                     $this->data['page'] = 'vacancies' ;
                     $this->load_view();
             }
             else
             {
                        $date = $this->input->post('deadline');
                        
                        $returndate =  new DateTime($date);                     

                     $data =array(
                             'job_id' => $this->input->post('job_id') , 
                             'people_needed' => $this->input->post('people_needed'),
                             //'deadline' => $returndate-> format('Y-m-d '),
                             'deadline' => (new DateTime($this->input->post('deadline')))-> format('Y-m-d '),
                                                    
                             'country' => $this->input->post('country')
                     );

                     if($this->app_model->AddNewVacancy($data) == TRUE)
                     {
                             redirect('index.php/admin/dashboard');
                             //echo $data['deadline'] ;
                             //echo  $returndate ; 
                             //echo $date ; 
                     }
                     else
                     {

                     }


             }
        }

        function date_valid($date)
        {
                $selecteddate = new DateTime($date);
                $month = (int) substr($date, 0, 2);
                $day = (int) substr($date, 3, 2);
                $year = (int) substr($date, 6, 4);
                
                $today = date('m/d/Y h:i:s a', time());
                
                
                if( checkdate($month, $day, $year))
                {
                        if($selecteddate <  new DateTime($today))
                        {
                                $this->form_validation->set_message('date_valid', "The date set is too late");
                                return FALSE ;
                        }
                        else
                        {
                                                                
                                return TRUE ; 
                        }
                }
                else
                {
                        $this->form_validation->set_message('date_valid', "Please enter a valid date" ); 
                        return FALSE ;

                }
                
                
        }

        public function removevacancy($id)
        {
                echo $id ;
                //update the selected newvacancy
                if($this->app_model->RemoveVacancy($id))
                {
                        redirect('index.php/admin/newvacancy') ;  
                }
                else
                {
                        echo "failed";
                         redirect('index.php/admin/newvacancy') ;  
                }
        }

        public function applications()
        {

        }

        public function homepage()
        {
                $this->data['homepage'] = $this->app_model->GetHomePages();
                $this->data['title'] ="Edite Home page" ; 
                $this->data['page'] = "edithomepage" ; 
                $this->load_view();
        }

        public function edithomepage()
        {
                $this->CheckLogin();

                $this->data['homepage'] = $this->app_model->GetHomePages();
                //set configuration for image uploads
                $config['upload_path'] = 'assets/img/homepage/'   ;
                $config['allowed_types'] = '*' ;
                $config['max_size'] = 10000; 
                $config['max_width'] = 1920 ; 
                $config['max_height'] = 1200 ;
                $config['overwrite'] = TRUE ;


                $validation_config = array(
                        array(
                                'field' => 'caption' , 
                                'label' => 'caption' , 
                                'rules' => 'required' , 
                                'errors' => array(
                                        'required' => 'Please enter a caption',
                                ),
                        ),
                );

                $this->form_validation->set_rules($validation_config) ; 
                $this->load->library('upload') ; 
                $this->upload->initialize($config);  

                if (  $this->form_validation->run() == False)
                {       
                        $this->data['title'] = 'Edit Home Page'    ;        
                        $this->data['page'] = 'edithomepage' ;
                        $this->load_view();
                }
                else
                {
                        if($this->input->post('replace_image') == True)
                        {
                                if( ! $this->upload->do_upload('image_path'))
                                {
                                        
                                        $this->data['error'] = $this->upload->display_errors() ;
                                        $this->data['title'] = 'Edit Home Page' ; 
                                        $this->data['page']  = 'edithomepage' ; 
                                        $this->load_view();
                                }
                                else
                                {

                                        $data['upload_data'] = $this->upload->data();


                                        $data = array(
                                                'homepage_id'=> $this->input->post('homepage_id') ,
                                                'caption' => $this->input->post('caption'),                                                                                                    
                                                'image_path' => $this->upload->data('file_name')   ,
                                                );  

                                        if( $this->app_model->UpdateHomepage($data))
                                        {
                                                redirect('index.php/admin/homepage') ;                                       
                                        }
                                        else
                                        {
                                               $this->data['error'] = $this->upload->display_errors() ;
                                                $this->data['title'] = 'Edit Home Page' ; 
                                                $this->data['page']  = 'edithomepage' ; 
                                                $this->load_view();
                                        }
                                }       
                        }
                        else
                        {
                                
                                $data = array(
                                                'homepage_id'=> $this->input->post('homepage_id') ,
                                                'caption' => $this->input->post('caption'),
                                                'image_path' => FALSE ,                                                                                                                                                    
                                        );  

                                        if( $this->app_model->UpdateHomepage($data))
                                        {
                                                redirect('index.php/admin/homepage') ;                                        
                                        }
                                        else
                                        {
                                                echo 'without failed' ;
                                        } 
                        }
                }

        }
}
?>
