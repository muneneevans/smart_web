<?php

class Forms extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('app_model');
        //$this->load->helper(array('form', 'url')) ; 
        $this->load->library('form_validation') ;
        $this->form_validation->set_error_delimiters('<div class="errors ">', '</div>');
        $this->load->library('session') ;
    }

    public function load_view()
    {

       // $this->load->view('templates/header',$this->data );
        $this->load->view('forms/'.$this->data['page'] ,$this->data ) ;
       // $this->load->view('templates/footer');
    }   

    public function signup()
    {
        
        //set rules in array
        $validation_config = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|callback_username_check'                
            ),
            array(
                'field' => 'password' ,
                'label' => 'Password' ,
                'rules' => 'required|min_length[5]|max_length[12]|is_unique[users.user_name]' , 
                'errors'=> array(
                    'required' => 'You must provide a %s.',
                    'is_unique'=> 'This %s already exists'
                ),
            ),
            array(
                'field' => 'passconf' , 
                'label' => 'Password Confirmation' , 
                'rules' => 'required|matches[password]',
                'errors'=> array(
                    'matches[password]' => 'The passwords do not match'
                ),
            ),
            array(
                'field' => 'email' , 
                'label' => 'Email' , 
                'rules' => 'trim|required|valid_email' 
            )
        );

        $this->form_validation->set_rules($validation_config) ; 

        if ( $this->form_validation->run() == False)
        {             
            $this->data['title'] = 'Sign Up'    ;        
            $this->data['page'] = 'signup' ;
            $this->load_view();
        }
        else
        {
            $data = array(
            'user_name' => $this->input->post('username'),    
            'password' => sha1( $this->input->post('password')),
            'email' => $this->input->post('email') );

            $result = $this->app_model->RegisterUser($data) ; 

            if($result == False)
            {
                echo 'unable to register' ;
            }
            else
            {
                echo 'register successful' ; 
            }
        }
    }

    public function login()
    {
        
        //set rules in array
        $validation_config = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'                
            ),
            array(
                'field' => 'password' ,
                'label' => 'Password' ,
                'rules' => 'required' , 
                'errors'=> array(
                    'required' => 'You must provide a %s.',
                    
                ),
            ),        
        );

        $this->form_validation->set_rules($validation_config) ; 

       
        if ( $this->form_validation->run() == False)
        {
            
            $this->data['title'] = 'Log in'    ;        
            $this->data['page'] = 'login' ;
            $this->load_view();
        }
        else
        {
            $data = array(
            'user_name' => $this->input->post('username'),    
            'password' => sha1( $this->input->post('password'))
            );

            $result = $this->app_model->LogIn($data) ;
            if( $result )
            {
                $this->session->set_userdata('user_name', $result[0]['user_name'] );                
                redirect('index.php/admin/');
            }
            else
            {
                $this->data['title'] = 'Log in'    ;        
                $this->data['page'] = 'login' ;
                $this->load_view();
                $this->form_validation->set_message('crederror', "invalid credentials");
            }

        }
    }

    public function username_check($str)
    {
        $data = array('user_name' => $str);
        if( ! $this->app_model->UsernameAvailable($data))
        {
            $errmsg = "{$str} is in use. Please enter another email " ;                
            $this->form_validation->set_message('username_check', $errmsg);
            return False ; 
        }                    
        else
        {
            return True ; 
        }
    }
}
?>