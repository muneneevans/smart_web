<?php
class Main extends CI_Controller {

        private $data ; 

        public function __construct()
        {
                parent::__construct();
                $this->load->model('app_model');
                $this->load->helper('url_helper');
                $this->load->library('form_validation') ;
                $this->form_validation->set_error_delimiters('<div class="errors ">', '</div>');
        }

        //function that loads every page
        public function load_view()
        {
            //load the header and footer and the requested page in between
            $this->load->view('templates/header',$this->data );
            $this->load->view('pages/'.$this->data['page']) ;
            $this->load->view('templates/footer');
            
        
        }   

        public function pagenotfound()        
        {
            $this->data['title'] = "Page Not Found" ; 
            $this->data['page']   = '404'  ; 
            $this->load_view();
        }

        public function index()
        {
            //navigate to the home page if no page is selected
            redirect('index.php/main/home') ;          
        }
        public function home()
        {
            //load the main page of the website(home)
            //get the images in the home screen
            $this->data['homepage'] = $this->app_model->GetHomePages();
            $this->data['title'] = 'Smart Applications International' ; 
            $this->data['page'] = 'home' ;
            $this->load_view() ;
        }
        public function services()
        {
            //load the services page
            $this->data['title'] = 'Services: Smart Applications International' ; 
            $this->data['page'] = 'services' ; 
            $this->load_view();
        }
        public function products()
        {
            //load the products page
            $this->data['title'] = 'Products Smart Applications' ; 
            $this->data['page'] = 'products' ; 
            $this->load_view();
        }
        public function about()
        {
            //load the about us page
            //load the clients page
            $this->data['title'] = 'Clients: Smart Applications International' ; 
            $this->data['page'] = 'about' ; 
            $this->load_view();
        }
        public function clients()
        {
            //load the clients page
            $this->data['title'] = 'Clients: Smart Applications International' ; 
            $this->data['page'] = 'clients' ; 
            $this->load_view();
        }
        

        public function news()
        {
            //load the news page

            //get the news stories from the database
            $this->data['news'] = $this->app_model->GetAllNews();
            $this->data['title'] = 'News Smart Applications International' ; 
            $this->data['page'] = 'news' ; 
            $this->load_view();
        }
        public function searchnews()
        {
            //filter news stories by the text the user has entered
            $this->data['news'] = $this->app_model->SearchNews($_POST['SearchText']) ;
            $this->data['title'] = 'News Smart Applications International' ; 
            $this->data['page'] = 'news' ; 
            $this->load_view();            
        }


        public function faqs()
        {
            //show the faqs
            $this->data['faqs'] = $this->app_model->GetFaqs();
            $this->data['title'] = "Frequently Asked Questions: Smart Applications International" ; 
            $this->data['page'] = 'faqs' ; 
            $this->load_view();
        }
        public function searchfaqs()
        {
            //filter news stories by the text the user has entered
            $this->data['faqs'] = $this->app_model->SearchFaqs($_POST['SearchText']) ;
            $this->data['title'] = 'Frequently Asked Questions: Smart Applications International' ; 
            $this->data['page'] = 'faqs' ; 
            $this->load_view();
        }

        public function howsmartworks()
        {
            //show the faqs            
            $this->data['title'] = "How Smart Works: Smart Applications International" ; 
            $this->data['page'] = 'howsmartworks' ; 
            $this->load_view();
        }


        public function contact()
        {
            //load the contact us page
            $this->data['title'] = 'Contact Us' ; 
            $this->data['page']  = 'contact' ; 
            $this->load_view();
        }

        public function sendmessage()
        {
            //allow the user to write a sendmessage
            $ci = get_instance();
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "munene.evansk@gmail.com"; 
            $config['smtp_pass'] = "i am evans";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $ci->email->initialize($config);

            

            //set configuration settings to validate the message form
            $validation_config = array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Please enter a name',
                    ),
                ),
                array(
                    'field' => 'email' , 
                    'label' => 'Email' ,
                    'rules'  => 'trim|required|valid_email',
                    'errors' => array(
                        'required' => 'please enter an email address',
                        'valid_email' => 'plese enter a valid email address',
                    ),
                ),
                array(
                    'field' => 'message' , 
                    'label' => 'Message' , 
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Please enter a message' ,
                    ),
                ),
            );

            $this->form_validation->set_rules($validation_config) ;

            //check if the rules of the form have been violated
            if( $this->form_validation->run() == FALSE)
            {
                //reload the page and show the errors
                $this->data['title'] = 'Contact Us' ; 
                $this->data['page']  = 'contact' ; 
                $this->load_view();
            }
            else
            {
                $ci->email->from('munene.evansk@gmail.com', 'Evans');
                    $list = array('munene.evansk@outlook.com');
                    $ci->email->to($list);
                    //$this->email->reply_to('my-email@gmail.com', 'Explendid Videos');
                    $ci->email->subject('New Message From Website');
                    $email_message = "From: ".$this->input->post('name')."\nEmail: ".$this->input->post('email')."\n\n Mesasge:\n".$this->input->post('message')."" ;
                    $ci->email->message($email_message);
                    $ci->email->send();
                redirect('index.php/main/sendmessage');

                   
                //get the user details fromt the form
                $data= array(
                    'name' => $this->input->post('name') , 
                    'email'=> $this->input->post('email'),
                    'message' => $this->input->post('message'),
                    'replied' => 'UNREAD'
                );

                //add message to the messages data base
                if($this->app_model->AddNewMessage($data))
                {
                    //reload the page
                    
                }
                else
                {
                    //show an error message and reload the page
                    echo "message sending failed" ;
                    redirect('index.php/main/sendmessage');
                }
            }
        }

        
        public function careers()
        {
            //display job vacancies if any
            $this->data['vacancies'] = $this->app_model->GetOpenVacancies() ; 
            $this->data['title'] = "Careers: Smart Applications International" ; 
            $this->data['page'] = 'careers' ; 
            $this->load_view();
        }

       
}

?>
