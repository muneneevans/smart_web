<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_model extends CI_Model {

    public function __construct()
    {
            $this->load->database();
    }

    public function GetAllNews()
    {
        $this->db->select('*');
        $this->db->from('news') ;   
        $this->db->order_by('date_published' , 'DESC');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function SearchNews($SearchText)
    {
        $query = $this->db->query(
		"SELECT *, ( MATCH (content ) AGAINST ('".$this->db->escape_str($SearchText)."' in boolean mode) ) AS score
		FROM news
		WHERE ( MATCH (content) AGAINST ('" .$this->db->escape_str($SearchText). "' in boolean mode) ) or ( MATCH (title) AGAINST ('" .$this->db->escape_str($SearchText). "' in boolean mode))
		ORDER BY score DESC"
		);

		if($query)
        {
             return $query->result_array() ;
        }
		else
        {
             return false ; 
        }
    }

    //get a story using a hash code
    public function GetStorySecure($data)
    {
        $condition = "sha1(news_id) =" . "'" . $data['id'] . "'" ;
        $this->db->select("*") ; 
        $this->db->from('news') ; 
        $this->db->where($condition) ; 
        $this->db->limit(1) ;
        $query = $this->db->get();
        if($query->num_rows() == 0 )
        {
            return False ;
        }
        else
        {
            return $query->result_array();
        }
    }

    public function UpdateStory($data)
    {
        $this->db->set('title' , $data['title']);
        $this->db->set('short_form' ,$data['short_form']);
        $this->db->set('content' ,$data['content']);
        if($data['banner_image'])
        {
            $this->db->set('banner_image' , $data['banner_image']);
        }
        $this->db->where('news_id' , $data['news_id']) ;
        $this->db->update('news') ;

        if ($this->db->affected_rows() > 0) 
        {
            return TRUE;
        }
        else
        {
            return FALSE ; 
        }

    }

    public function LogIn($data)
    {
        $condition = "user_name =" . "'" . $data['user_name'] . "' AND password = " . "'" .$data['password']. "'";
       
        $this->db->select('*') ; 
        $this->db->from('users');
        $this->db->where($condition) ; 
        $this->db->limit(1) ;
        $query = $this->db->get();
        if($query->num_rows() == 0)
        {
           return false ; 
        }
        else
        {
            return $query->result_array();
        }
    }

    public function RegisterUser($data)
    {
        $condition = "user_name =" . "'" . $data['user_name'] . "'";
        $this->db->select('*') ; 
        $this->db->from('users');
        $this->db->where($condition) ; 
        $this->db->limit(1) ;
        $query = $this->db->get();
        if($query->num_rows() == 0)
        {
            $this->db->insert('users', $data) ;
            if ($this->db->affected_rows() > 0) 
            {
                return true;
            }
        }
        else
        {
            return false;
        }
        
    }

    public function UsernameAvailable($data)
    {
        $condition = "user_name =" . "'" . $data['user_name'] . "'";
        $this->db->select('*') ; 
        $this->db->from('users');
        $this->db->where($condition) ; 
        $this->db->limit(1) ;
        $query = $this->db->get();

        if($query->num_rows() == 0)
        {           
            return true ;
        }
        else
        {
            return false;
        }
        
    }

    public function AddNewStory($data)
    {
            $this->db->insert('news', $data) ;
            if ($this->db->affected_rows() > 0) 
            {
                return True;
            }
            else
            {
                return False ;
            }

    }

    public function AddNewMessage($data)
    {
        $this->db->insert('message', $data) ;
            if ($this->db->affected_rows() > 0) 
            {
                return True;
            }
            else
            {
                return False ;
            }

    }

    public function GetUnreadMessages()
    {
        $condition = "replied = 'UNREAD' " ; 
        $this->db->select('*') ;
        $this->db->from('message') ; 
        $this->db->where($condition) ; 
        $this->db->limit(3) ;
        $query = $this->db->get();
        return $query->result_array();
    }

    public function GetAllMessages()
    {
        $this->db->select('*') ;
        $this->db->from('message') ; 
        $this->db->order_by('replied', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
	
    public function AddNewJob($data)
    {
        $this->db->insert('jobs', $data) ;
        if ($this->db->affected_rows() > 0) 
        {
            return True;
        }
        else
        {
            return False ;
        }
    }

    public function GetAlljobs()
    {
         $query = $this->db->get('jobs');
         if($query->num_rows() == 0)
        {
           return false ; 
        }
        else
        {
            return $query->result_array();
        }
    }

    public function AddNewVacancy($data)
    {
        
         $this->db->insert('vacancies', $data) ;
        if ($this->db->affected_rows() > 0) 
        {
            return True;
        }
        else
        {
            return False ;
        }
    }

    public function GetAllVacancies()
    {
        $condition = "WHERE jobs.job_id = vacancies.job_id " ;
        $this->db->select("*") ; 
        $this->db->from('jobs') ; 
        $this->db->join('vacancies', 'vacancies.job_id = jobs.job_id');        
        $query = $this->db->get();
        if($query->num_rows() == 0 )
        {
            return False ;
        }
        else
        {
            return $query->result_array();
        }
    }

    public function GetOpenVacancies()
    {
        date_default_timezone_set('Africa/Nairobi');
        $today = date('Y/m/d', time());
        

        $this->db->select("*") ; 
        $this->db->from('jobs') ;         
        
        $this->db->where('status', 'EMPTY');
        $this->db->where('deadline >', $today);
        $this->db->join('vacancies', 'vacancies.job_id = jobs.job_id');        
        $query = $this->db->get();
        if($query->num_rows() == 0 )
        {
            return False ;
        }
        else
        {
            return $query->result_array();
        }
    }
    public function GetEmptyVacancies()
    {        
        $this->db->select("*") ; 
        $this->db->from('jobs') ;                 
        $this->db->where('status', 'EMPTY');
        $this->db->join('vacancies', 'vacancies.job_id = jobs.job_id');        
        $query = $this->db->get();
        if($query->num_rows() == 0 )
        {
            return False ;
        }
        else
        {
            return $query->result_array();
        }
    }

    public function RemoveVacancy($id)
    {
        $this->db->set('status' , 'FILLED');
        $this->db->where('vacancy_id' , $id) ;
        $this->db->update('vacancies') ;

        if ($this->db->affected_rows() > 0) 
        {
            return TRUE;
        }
        else
        {
            return FALSE ; 
        }

    }

    public function GetFaqs()
    {
        $query = $this->db->get('faqs');
         if($query->num_rows() == 0)
        {
           return false ; 
        }
        else
        {
            return $query->result_array();
        }
    }

    public function SearchFaqs($SearchText)
    {
        $query = $this->db->query(
		"SELECT *, ( MATCH (question ) AGAINST ('".$this->db->escape_str($SearchText)."' in boolean mode) ) AS score
		FROM faqs
		WHERE ( MATCH (answer) AGAINST ('" .$this->db->escape_str($SearchText). "' in boolean mode) ) or ( MATCH (question) AGAINST ('" .$this->db->escape_str($SearchText). "' in boolean mode)) or ( MATCH (tags) AGAINST ('" .$this->db->escape_str($SearchText). "' in boolean mode))
		ORDER BY score DESC"
		);

		if($query)
        {
             return $query->result_array() ;
        }
		else
        {
             return false ; 
        }
    }

    public function GetHomePages()
    {
        $this->db->select('*') ; 
        $this->db->from('homepageheader');
        $this->db->limit(4) ;
        $query = $this->db->get();
        if($query->num_rows() == 0)
        {
           return false ; 
        }
        else
        {
            return $query->result_array();
        }
    }

    public function UpdateHomepage($data)
    {

        $this->db->set('caption' , $data['caption']);        
        if($data['image_path'])
        {
            $this->db->set('image_path' , $data['image_path']);
        }
        $this->db->where('homepage_id' , $data['homepage_id']) ;
        $this->db->update('homepageheader') ;

        if ($this->db->affected_rows() > 0) 
        {
            return TRUE;
        }
        else
        {
            return FALSE ; 
        }        
    }
   
}
