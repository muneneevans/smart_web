    <div class="col-sm-9 admin-content">
        <h1> Edit home page </h1>
        
        <?php foreach($homepage as $home)
        {?>
            <form id="form1" class="col-md-4 col-sm-6"action="<?php echo base_url('/index.php/')?>admin/edithomepage" enctype="multipart/form-data" method="post" accept-charset="utf-8"  >
                
                <div class="form-group">
                    <img class="img img-responsive fill admin-homepage-image" id="<?php echo $home['homepage_id']?>" alt="your image"  src="<?php echo base_url();?>assets/img/homepage/<?php echo $home['image_path'] ;?>"/>
                </div>
                
                <div class="form-group">                        
                    <h5>Replace image</h5>
                    <input type="checkbox" name="replace_image"/>                                             
                </div>
                
                <div class="form-group">                    
                    <input type='file' name="image_path" onchange="readURL(this , <?php echo $home['homepage_id'];?>);"  />
                </div>
                
                <div class="form-group">
                    <h5>caption</h5>
                    <input type="hidden" name="homepage_id" value="<?php echo $home['homepage_id'];?>"/>
                    <?php echo form_error('caption'); ?>
                    <textarea class="trumbo-small" name="caption"><?php echo $home['caption'];?></textarea>
                    
                    <input type="submit"  class="btn btn-info" value="update"/>
                </div>
            </form>
        
        <?php 
        }?>

</div>  
</div>