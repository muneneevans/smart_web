<div class=" col-sm-9  admin-content">

    <div class="col-sm-9  container">
         <form class="form admin-form" action="<?php echo base_url('/index.php/')?>admin/updatestory" enctype="multipart/form-data" method="post" accept-charset="utf-8" id="updatestoryform">

                <div class="form-group">
                    <h5>Title</h5>
                    <?php echo form_error('title'); ?>
                    <input class="form-control" type="text" name="title" value="<?php echo set_value('story_title');?>"/>
                </div>

                <div class="form-group">
                    <h5>Brief</h5>
                    <?php echo form_error('short_form'); ?>
                    <input class="form-control" type="text" name="short_form" value="<?php echo set_value('short_form');?>"/>
                </div>

                
                <div class= "form-group">
                    
                </div>
                <div class="form-group">                        
                    <h5>Replace image</h5>
                    <input type="checkbox" name="replace_image"/>     
                    <?php echo form_error('banner_image'); ?>         
                    <input type="file" name="banner_image" size="20" >
                    
                </div>

                <div class="form-group content-area">
                    <h5>Content</h5>
                    <?php echo form_error('content'); ?>
                    <textarea class="form-control trumbo" rows="10" columns="50" name="content" > <?php echo set_value('content');?></textarea>
                </div>
                

                <input type="submit" class="btn btn-primary btn-large" value="update story">
                <input type="hidden" name="news_id" value="<?php echo set_value('news_id');?>"/>
            </form>
    </div>
</div>

</div>