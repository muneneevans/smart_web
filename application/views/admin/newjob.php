<div class="col-sm-9 admin-content">
    <div class="col-sm-9  container">
         <form class="form admin-form" action="<?php echo base_url('/index.php/')?>admin/newjob" enctype="multipart/form-data" method="post" accept-charset="utf-8" id="newjobform">
            
            <div class="form-group">
                <h5>Title</h5>
                <?php echo form_error('title'); ?>
                <input class="form-control" type="text" name="title" value="<?php echo set_value('title');?>"/>
            </div>

            <div class="form-group content-area">
                <h5>Description</h5>
                <?php echo form_error('description'); ?>
                <textarea  class="form-control trumbo" rows="5" columns="50" name="description" value="<?php echo set_value('description');?>" ></textarea>
            </div>

            <div class="form-group content-area">
                <h5>Skills</h5>
                <?php echo form_error('skills'); ?>
                <textarea  class="form-control trumbo" rows="5" columns="50" name="skills" value="<?php echo set_value('skills');?>" ></textarea>
            </div>

            <input type="submit" class="btn btn-primary btn-large" value="add new job">
         </form>
    </div>
</div>


</div>