
<div class=" col-sm-9 admin-content flex-center">

    <div class="col-sm-9 flex-center ">
         <form class="form admin-form" action="<?php echo base_url('/index.php/')?>admin/newstory" enctype="multipart/form-data" method="post" accept-charset="utf-8" id="newstoryform">

                <div class="form-group">
                    <h5>Title</h5>
                    <?php echo form_error('title'); ?>
                    <input class="form-control" type="text" name="title" value="<?php echo set_value('title');?>"/>
                </div>

                <div class="form-group">
                    <h5>Brief</h5>
                    <?php echo form_error('short_form'); ?>
                    <input class="form-control" type="text" name="short_form" value="<?php echo set_value('short_form');?>"/>
                </div>

                <div class="form-group">
                    <h5>Image</h5>           
                    <?php echo form_error('banner_image'); ?>         
                    <input  type="file" name="banner_image" size="20" value="<?php echo set_value('banner_image');?>" >
                    
                </div>

                <div class="form-group content-area">
                    <h5>Content</h5>
                    <?php echo form_error('content'); ?>
                    <textarea  class="form-control  trumbo" rows="10" columns="50" name="content" value="<?php echo set_value('content');?>" ></textarea>
                </div>
                
                

                <input type="submit" class="btn btn-primary btn-large" value="add new story">

            </form>
    </div>
</div>

</div>