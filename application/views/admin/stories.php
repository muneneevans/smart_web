<?php?>

    <div class="col-sm-10  admin-content">
        <div>
            <div class="text-center">
                <h2> News Stories </h2>
                <table class="table  table-condensed ">
                    <thead>
                        <tr>
                            <th> </th>
                            <th> title </th>
                            <th> Summary </th>
                            <th> last updated </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($news as $newsitem)
                        {?>
                            <tr>
                                <td>
                                    <a class=" " 
                                    href="<?php echo base_url('/index.php/')?>admin/editstory/<?php echo  sha1($newsitem['news_id'] ); ?>"> edit </button>
                                </td>
                                <td>
                                    <p class="content-row">
                                        <?php echo $newsitem['title'] ?>
                                    </p>
                                </td>
                                <td  >
                                    <p class="content-row">
                                        <?php echo $newsitem['short_form'] ?>
                                    </p>
                                </td>
                                <td>
                                    <p class="content-row">
                                        <?php echo $newsitem['date_updated'] ?>
                                    </p>
                                </td>
                                 
                            </tr>
                        <?php
                        }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>