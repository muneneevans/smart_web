<div class="col-sm-9  admin-content page-section">    
    <div class="col-sm-4  border-box container-fluid">
        <form class="form admin-form container-fluid" action="<?php echo base_url('/index.php/')?>admin/newvacancy" enctype="multipart/form-data" method="post" accept-charset="utf-8" id="newvacancyform">
            
            <div class="form-group container-fluid">
                <h5>Job type</h5>
                <?php echo form_error('job'); ?>
                <select name="job_id" class="form-control" >
                    <?php foreach($jobs as $job)
                    {?>
                        <option value="<?php echo $job['job_id']?>" >
                            <?php echo $job['title']?>
                        </option>
                    <?php
                    }?>
                </select>
            </div>

            <div class="form-group container-fluid">
                <h5>People Needed</h5>
                <?php echo form_error('people_needed'); ?>
                <input class="form-control" type="number" min="1" name="people_needed" value="<?php echo set_value('people_needed');?>"/>
            </div>

            <div class="form-group container-fluid">
                <h5>Country</h5>
                <?php echo form_error('country'); ?>
                <input  class="form-control " type="text"  name="country" value="<?php echo set_value('country');?>"/>
            </div>

            <div class="form-group container-fluid">
                <h5>Expiry date</h5>
                <?php echo form_error('deadline'); ?>
                <input class="form-control datepicker" type="date"  name="deadline" value="<?php echo set_value('deadline');?>"/>
            </div>

            <div class="form-group container-fluid">
                <input type="submit" class="btn btn-primary btn-large" value="submit vacancy">
            </div>

        </form>
    </div>

    <div class="col-sm-8 container ">
        <table class="table border-box">
            <thead>
                <tr>                    
                    <th>Action</th>
                    <th>tile</th>
                    <th>number</th>
                    <th>Country</th>
                    <th>Deadline</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($emptyjobs)
                    {
                        foreach($emptyjobs as $job)
                        {?>
                            <tr>
                                
                                    <td><a  href ="<?php echo base_url('/index.php/'); ?>admin/removevacancy/<?php echo $job['vacancy_id'];?>">remove </a></td>                                
                                    <td><?php echo $job['title'] ;?></td>
                                    <td><?php echo $job['people_needed'] ;?></td>
                                    <td><?php echo $job['country'] ;?></td>
                                    <td><?php echo $job['deadline'] ;?></td>
                                
                            </tr>
                        <?php
                        }
                    }
                    else
                    {
                        ?>
                        <p> There are no empty jobs</p>
                        <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
</div>