<head>
		<title><?php echo $title; ?></title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="Smart Applications">

	
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/favicon.ico" />
		<link rel="stylesheet" type="text/css" media="screen"  href="<?php echo base_url();?>assets/css/bootstrap.min.css" >
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/style.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/mdb.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/mdb.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/bootstrap-theme.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/trumbowyg.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/trumbowyg.min.css">
		

		<script src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/progressbar.js"></script>
		<script src="<?php echo base_url();?>assets/js/smart.js"></script>
		<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/mdb.js"></script>
		<script src="<?php echo base_url();?>assets/js/npm.js"></script>
		<script src="<?php echo base_url();?>assets/js/trumbowyg/trumbowyg.js"></script>
		<script src="<?php echo base_url();?>assets/js/trumbowyg/trumbowyg.min.js"></script>
		
	</head>
 <?php echo form_error('crederror'); ?>

<div class="container">
    
    <div class=" container col-md-4 col-md-offset-4  container card ">
        <div>
            <img class="img img-responsive" src="<?php echo base_url();?>assets/img/smart_logo_large.jpg"   style="max-height: 200px ;"/>
        </div>
        <form class="form" action="<?php echo base_url('/index.php/');?>forms/signup" method="post" accept-charset="utf-8">

                <div class="form-group">
                    <h5>Username</h5>
                    <?php echo form_error('username'); ?>
                    <input type="text" name="username" value="<?php echo set_value('username');?>"/>
                </div>

                <div class="form-group">                    
                    <h5>Password</h5>
                    <?php echo form_error('password'); ?>
                    <input type="password" name="password" value="<?php echo set_value('password');?>"/>
                </div>

                <div class="form-group">
                    <h5>Password Confirm</h5>
                    <?php echo form_error('passconf'); ?>
                    <input type="password" name="passconf" value="<?php echo set_value('passconf');?>"/>
                </div>

                <div class="form-group">
                    <h5>Email Address</h5>
                    <?php echo form_error('email'); ?>
                    <input type="text" name="email" value="<?php echo set_value('email');?>"/>
                </div>
                
                <div class="form-group">
                    <input type="submit" value="sign up" />
                </div>
            
        </form>
    </div>
</div>
