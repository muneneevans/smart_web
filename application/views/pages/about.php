
<div class="page-section-red ">
	<div class="container">
    	<h1 class="row text-center">About Us</h1>
	</div>
</div>

<div class="page-section-dark ">
    <div class="container card">
        <div class="page-small-section-white row">
            <img class="img img-responsive img-fluid fill " style="height: 200px ;" src="<?php echo base_url();?>assets/img/banner2-2.png"/>
        </div>
        <div class="page-small-section-white row text-justify padding-box-xs">
            <div class=" padding-box-xs">
                <h1 class="section-xs">Who we are</h1>
                <h3>
                    Smart Applications International is a leading ICT solutions provider delivering a wide range of world class technological solutions.
                    Fondly known as Smart within the industry, the company was founded to provide innovative, secure and high-tech solution in Africa and beyond. Staying ahead through constant research and innovation.
                    We offer solutions that are secure, relevant and convenient.

                </h3>
            </div>
            <div class=" padding-box-xs">
                <h1 class="">Our Vision</h1>
                <h3>
                To make access to essential services easier and faster for everyone through secure technology solutions
                </h3>
            </div>
            <div class=" padding-box-xs">
                <h1 class="">Our Mission</h1>
                <h3>
                    Creating a society of healthy people through easier, simpler and faster access to healthcare services
                </h3>
            </div>
            <div class=" padding-box-xs">
                <h1 class="">Our Values</h1>                
                <div class="">
                    <h4>we are</h4>   
                    <table>
                        <tr>
                            <td><b>Ethical</b></td>
                            <td>esponsible, Reliable, Secure</td>                        
                        </tr>
                        <tr>
                            <td><b>Accessible</b></td>
                            <td>Have Reach, Responsive, Efficient</td>                        
                        </tr>
                        <tr>
                            <td><b>Smart</b></td>
                            <td>Cutting Edge, Relevant, Adaptive</td>                        
                        </tr>
                        <tr>
                            <td><b>You</b></td>
                            <td>We understand You, We are about You</td>                        
                        </tr>
                    </table>
                <div class=" center-children">
                    <div class="col-sm-3 col-xs-6">
                        <div class="easy-circle  ">
                            <div class="easy-circle-top-tag center-children ">
                                <h4 class="fill">Ethical</h4>
                            </div>
                            <div class="easy-circle-bottom-tag center-children">
                                <h5 class="fill">Responsible</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-3 col-xs-6 ">
                        <div class="easy-circle  ">
                            <div class="easy-circle-top-tag center-children ">
                                <h4 class="fill">Accessible</h4>
                            </div>
                            <div class="easy-circle-bottom-tag center-children">
                                <h5 class="fill">Convenient</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-3  col-xs-6">
                        <div class="easy-circle  ">
                            <div class="easy-circle-top-tag center-children ">
                                <h4 class="fill">Smart</h4>
                            </div>
                            <div class="easy-circle-bottom-tag center-children">
                                <h5 class="fill">Cutting edge</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-3 col-xs-6 ">
                        <div class="easy-circle  ">
                            <div class="easy-circle-top-tag center-children ">
                                <h4 class="fill">You</h4>
                            </div>
                            <div class="easy-circle-bottom-tag center-children">
                                <h5 class="fill">we are about you</h5>
                            </div>
                        </div>
                    </div>
                </div>  
                <h1 class="row text-center keywords">EASY</h1>
            </div>
        </div>
        </div>
    </div>
</div>



