<div class="page-section-red cender-children">
    <div class="container">
        <h1 class="row text-center">Careers</h1>
    </div>
</div>

<div class="page-section-dark ">
    <div class="container">
        <div class="page-small-section-white row">
            <img class="img img-responsive img-fluid fill " style="height: 300px ;" src="<?php echo base_url();?>assets/img/work.jpg"/>
        </div>
        <div class="page-small-section-white row text-justify padding-box-xs card">
            <div class=" padding-box-xs">
                <h1 class="section-xs">Why work at Smart</h1>
                <h3>
                    We pride ourselves in creating exceptional experiences for our customers through transformative products, services and technologies that are Secure, Fast and Accessible.
                    Our vibrant community builds solid relationships on mutual respect, satisfying the needs of our customers and building a strong future for us all.
                </h3>
                <h3>
                    Come join us. We have great teams in place and we are always looking for more!

                </h3>
            </div>
            <div class=" padding-box-xs">
                <h1 class="">Vacancies</h1>
                <?php
                    //check if there are any vacancies
                    if($vacancies == FALSE)
                    {
                ?>
                    <div >
                        <h4>We have no vacancies at the moment. Keep visiting our website for vacancies in the future.</h4>
                    </div>
                <?php
                    }
                    else
                    {
                ?>      
                <div class="Section-xs">                     
                    <div>
                        <h4>
                            Before making any application, please make sure you have the following documents in soft copy
                        </h4>
                        <h4>
                            <li class="keywords">Resume</li>
                            <li class="keywords">Academic Certificates</li>
                            <li class="keywords">Testimonials </li>
                        </h4>

                    </div>
                        <div class="">
                          
                        <?php
                            foreach($vacancies as $vacancy)
                            {
                        ?>
                            <div class="container-fluid section-xxs">
                                <div class="container-fluid border-box">
                                    <a data-toggle="collapse" data-target="#<?php echo $vacancy['vacancy_id'];?>div">
                                        <div class="container-fluid row">
                                            <div class="col-sm-7 col-xs-6">
                                                <h4><?php echo $vacancy['title'] ;?></h4>
                                            </div>
                                            <div class="col-sm-4 col-xs-6 container-fluid hidden-xs">
                                                
                                                <div class="col-xs-4 container-fluid hidden-xs">
                                                    <h4><?php echo $vacancy['country'] ;?></h4>
                                                </div>
                                                <div class="col-xs-5 container-fluid">
                                                    <h4><?php echo $vacancy['deadline'] ;?></h4>
                                                </div>
                                                <div class="col-xs-3 container-fluid">
                                                    <a class="btn btn-cyan"   >Apply</a>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div id="<?php echo $vacancy['vacancy_id'];?>div" class="container-fluid collapse">
                                        
                                        <div class=" container-fluid row">
                                            <h4><?php echo $vacancy['country'] ;?></h4>
                                        </div>
                                        <div class="container-fluid row">
                                            <h4><?php echo $vacancy['deadline'] ;?></h4>
                                        </div>
                                        <div class="container-fluid row">                                            
                                            <?php echo $vacancy['skills'] ;?>                                                
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        <?php
                            }
                        ?>

                           
                        </div>
                    </div>
                <?php
                    }
                ?>
                </div>
            
        </div>

        
    </div>
</div>










