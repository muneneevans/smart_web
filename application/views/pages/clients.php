
<div class="page-section-red cender-children">
    <div class="container">
        <h1 class="row text-center">Clients</h1>
    </div>
</div>

<div class="page-section">
    <div class="container">
        
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#insurance_tab">Insurance</a></li>
            <li><a data-toggle="tab" href="#banks_tab">Banks</a></li>
            <li><a data-toggle="tab" href="#government_tab">Government</a></li>
            <li><a data-toggle="tab" href="#others_tab">General</a></li>
        </ul>

        <div class="tab-content">
 
            <div id="insurance_tab" class="tab-pane fade in active">
                <h3>Insurance companies</h3>
                <p>
                    Smart works together with Insurance companies in the automation and management of medical schemes through the biometric smart card solution.
                </p>

                <div class="container">
                    <div class="row">
                        <div class="container client-logo-container center-children">
				
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/jubilee.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img img-responsive fill" src="<?php echo base_url();?>assets/img/logos/aon.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/apa.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/britam.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/uap.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/cic.jpg"/>
                            </div>


                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/aar_insurance.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/afriq_insurance.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/first_assurance.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/madison.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/general_assurance.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/resolution.jpg"/>
                            </div>

                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/heritage_insurance.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/eagle_africa.jpg"/>
                            </div>
			            </div>
                    </div>
                </div>
            </div>

            <div id="banks_tab" class="tab-pane fade">
                <h3>Banks</h3>                
                <div class="container">
                    <div class="row">
                        <div class="container client-logo-container center-children">
				
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/kcb.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img img-responsive fill" src="<?php echo base_url();?>assets/img/logos/equity.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/sidian.jpg"/>
                            </div>
                            
			            </div>
                    </div>
                </div>
            </div>

            <div id="government_tab" class="tab-pane fade">
                <h3>Governmental institutions</h3>

                <div class="container">
                    <div class="row">
                        <div class="container client-logo-container center-children">
				
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/nssf.PNG"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img img-responsive fill" src="<?php echo base_url();?>assets/img/logos/kengen.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/kplc.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/kra.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/nairobi_water.jpg"/>
                            </div>
                            
			            </div>
                    </div>
                </div>
            </div>
            
            <div id="others_tab" class="tab-pane fade">
                <h3>Other Institutions</h3>
                
                <div class="container">
                    <div class="row">
                        <div class="container client-logo-container center-children flex-box">
				
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/cooperative_university_college.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img img-responsive fill" src="<?php echo base_url();?>assets/img/logos/gertrudes_hospital.jpg"/>
                            </div>
                                <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                    <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/kenya_airways.png"/>
                                </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/kobil.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/sanlam.jpg"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/delloitte.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/kpmg.png"/>
                            </div>
                            <div class="col-md-3 client-image col-sm-2 col-xs-3 ">
                                <img class="img  img-responsive fill" src=" <?php echo base_url();?>assets/img/logos/pwc.png"/>
                            </div>
                            
			            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>