<?php ?>
<div class="page-section-red cender-children">
    <div class="container">
        <h1 class="row text-center">Contact Us</h1>
    </div>
</div>
<div>
    <div class="page-section-dark container" style="margin-top: 10px; ">
        <div>
            <h2 class="text-center">Leave us a message</h2>
            <div class="container-fluid col-sm-6 col-sm-offset-3" >
                <form class="form" action="<?php echo base_url('/index.php/');?>main/sendmessage" method="post" accept-charset="utf-8">

                    <div class="form-group ">
                        <?php echo form_error('name'); ?>
                        <input placeholder="Name" class="form-control" type="text" name="name" value="<?php echo set_value('name');?>"/>
                    </div>
                    <div class="form-group">
                       
                        <?php echo form_error("email"); ?>
                        <input placeholder="Email" class="form-control" type="text" name="email" value="<?php echo set_value('email');?>"/>
                    </div>
                    <div class="form-group">
                        
                        <?php echo form_error("message") ;?>
                        <textarea class="form-control" name="message" rows="5" columns="50"><?php echo set_value('message');?></textarea>
                    </div>
                    <div class="form-group row">
                        <div class="">
                            <input placeholder="Message" class="btn btn-info btn-large  " type=submit value="Send"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container page-section">
        <div class="row">

            <!--Kenya-->
            <div class="col-sm-6">
                <div>
                    <div class="col-sm-4 col-xs-4">
                        <img class="img img-responsive img-flag" src="<?php echo base_url();?>assets/img/flags/kenya.png"/>
                    </div>
                    <div class="col-sm-8 col-xs-8 keywords-underline">
                        <h2> KENYA</h2>
                    </div>
                </div>

                <div>
                    <div>
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/building.png"/>
                                    </td>
                                    <td>
                                        <p> International House, Mama Ngina Street</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/postal.png"/>
                                    </td>
                                    <td>
                                        <p> P.O. Box: 57776 - 00200, Nairobi,</p>
                                        <p> Kenya</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/telephone.png"/>
                                    </td>
                                    <td>
                                        <p> +254 20 320 6000</p><a href="tel:+254 20 320 6000">(click to call)</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/cellphone.png"/>
                                    </td>
                                    <td>
                                        <p> +254 733 320 600, +254 718 222 000</p>
                                    </td>
                                </tr>                                

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/email.png"/>
                                    </td>
                                    <td>
                                        <a href="mailto:info@smartapplicationsgroup.com" ><p>  info@smartapplicationsgroup.com</p></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--Uganda-->
            <div class="col-sm-6">
                <div>
                    <div class="col-sm-4 col-xs-4">
                        <img class="img img-responsive img-flag" src="<?php echo base_url();?>assets/img/flags/uganda.png"/>
                    </div>
                    <div class="col-sm-8 col-xs-8 keywords-underline">
                        <h2> UGANDA</h2>
                    </div>
                </div>

                <div>
                    <div>
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/building.png"/>
                                    </td>
                                    <td>
                                        <p> Nakawa Business Park Block A 3rd Floor</p>
                                        <p>Plot 3-5 Port Bell Road</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/postal.png"/>
                                    </td>
                                    <td>
                                        <p> P.O. Box 33947,</p>
                                        <p> Kampala</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/telephone.png"/>
                                    </td>
                                    <td>
                                        <p> +256 312 555 300, +256 414 257 070, +256 703 251 52</p> <a href="tel:+256 312 555 300">(click to call)</a>
                                    </td>
                                </tr>                                                           

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/email.png"/>
                                    </td>
                                    <td>
                                        <a href="mailto:info.ug@smartapplicationsgroup.com"><p>  Info.ug@smartapplicationsgroup.com</p></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <!--South Sudan-->
            <div class="col-sm-6">
                <div>
                    <div class="col-sm-4 col-xs-4">
                        <img class="img img-responsive img-flag" src="<?php echo base_url();?>assets/img/flags/south_sudan.png"/>
                    </div>
                    <div class="col-sm-8 col-xs-8 keywords-underline">
                        <h2> SOUTH SUDAN</h2>
                    </div>
                </div>

                <div>
                    <div>
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/building.png"/>
                                    </td>
                                    <td>
                                        <p> Tong Ping, Next To USA Residence</p>
                                        <p>Plot 51 Off Airport Road</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/postal.png"/>
                                    </td>
                                    <td>
                                        <p> P.O. Box 113,</p>
                                        <p> Juba</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/telephone.png"/>
                                    </td>
                                    <td>
                                        <p> +211 977 901 720, +211 977 786 081</p> <a href="tel:+211 977 901 720">(click to call)</a>
                                    </td>
                                </tr>                                                           

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/email.png"/>
                                    </td>
                                    <td>
                                        <a href="mailto:info.ss@smartapplicationsgroup.com"> <p> info.ss@smartapplicationsgroup.com <p> </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--Tanzania-->
            <div class="col-sm-6">
                <div>
                    <div class="col-sm-4 col-xs-4">
                        <img class="img img-responsive img-flag" src="<?php echo base_url();?>assets/img/flags/tanzania.png"/>
                    </div>
                    <div class="col-sm-8 col-xs-8 keywords-underline">
                        <h2>TANZANIA</h2>
                    </div>
                </div>

                <div>
                    <div>
                        <table class="table table-condensed">
                            <tbody>
                                
                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/postal.png"/>
                                    </td>
                                    <td>
                                        <p> P.O. Box 7966,</p>
                                        <p> Dar Es Salaam,</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>                                    
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/telephone.png"/>
                                    </td>
                                    <td>
                                        <p> +255 783 013 599</p> <a href="tel:+255 783 013 599">(click to call)</a>
                                    </td>
                                </tr>                                                           

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/email.png"/>
                                    </td>
                                    <td>
                                        <a href="mailto:info.tz@smartapplicationsgroup.com"> <p>info.tz@smartapplicationsgroup.com</p> </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!--Rwanda-->
            <div class="col-sm-6">
                <div>
                    <div class="col-sm-4 col-xs-4">
                        <img class="img img-responsive img-flag" src="<?php echo base_url();?>assets/img/flags/rwanda.png"/>
                    </div>
                    <div class="col-sm-8 col-xs-8 keywords-underline">
                        <h2>RWANDA</h2>
                    </div>
                </div>
                <div>
                    <div>
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/building.png"/>
                                    </td>
                                    <td>
                                        <p> RSSB Building, Tower 2, 8th Floor  - Wing A</p>
                                        <p>KN 3 RD, African Union Boulevard</p>                                        
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/postal.png"/>
                                    </td>
                                    <td>
                                        <p> P.O Box 6019,</p>
                                        <p> Kigali, Rwanda</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/telephone.png"/>
                                    </td>
                                    <td>
                                        <p> +250 787 780 027, +250 784 015 759</p> <a href="tel:250 787 780 027">(click to call)</a>
                                    </td>
                                </tr>                                                           

                                 <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/cellphone.png"/>
                                    </td>
                                    <td>
                                        <p> +250 789 772 271</p>
                                    </td>
                                </tr> 

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/email.png"/>
                                    </td>
                                    <td>
                                        <a href="mailto:info.rw@smartapplicationsgroup.com" > <p>  Info.rw@smartapplicationsgroup.com</p> </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--Zambia-->
            <div class="col-sm-6">
                <div>
                    <div class="col-sm-4 col-xs-4">
                        <img class="img img-responsive img-flag" src="<?php echo base_url();?>assets/img/flags/zambia.png"/>
                    </div>
                    <div class="col-sm-8 col-xs-8 keywords-underline">
                        <h2>ZAMBIA</h2>
                    </div>
                </div>

                <div>
                    <div>
                        <table class="table table-condensed">
                            <tbody>
                                
                                 <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/building.png"/>
                                    </td>
                                    <td>
                                        <p> A501 Northgate Gardens, NAPSA Complex</p>                                    
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/postal.png"/>
                                    </td>
                                    <td>
                                        <p> P.O Box 31355,</p>
                                        <p> Lusaka</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>                                    
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/telephone.png"/>
                                    </td>
                                    <td>
                                        <p>  +260 961 256 489</p><a href="tel:+260 961 256 489">(click to call)</a>
                                    </td>
                                </tr>                                                           

                                <tr>
                                    <td>
                                        <img class="contact-icon"  src="<?php echo base_url();?>assets/img/icons/contact/email.png"/>
                                    </td>
                                    <td>
                                        <a href="mailto:info.za@smartapplicationsgroup.com"><p> info.za@smartapplicationsgroup.com</p></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>