<div class="page-section-red cender-children">
	<div class="container">
    	<h1 class="row text-center">Frequently Asked Questions</h1>
	</div>
</div>

<div class="page-section">
    <div class="container">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="section-header">
                <h3>
                    Get answers to questions relating to use and support of smart systems
                </h3>
            </div>
            <div>
                <form class="form form-inline " action="<?php echo base_url('/index.php/main/searchfaqs')?>" method="POST">
                    <input class="form-control" type="text" name="SearchText"  placeholder="type and press enter"/>
                    <a class="" href="<?php echo base_url('main/faqs');?>">show all </a>
                </form>
            </div>
            <div>
                <?php
                    foreach($faqs as $faq)
                    {
                        ?>
                        <div class="faq-div">
                            <h4 class="faq-question">
                                <?php echo $faq['question']?>
                            </h4>
                            <p>
                                <?php echo $faq['answer']?>
                            </p>
                        </div>
                        <hr/>
                        <?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>