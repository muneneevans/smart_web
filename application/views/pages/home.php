<?php?>

        <!--the banner section-->
		<header id="banner" class="carousel slide carousel-fade" data-ride="carousel">

			<!-- circular indicators-->
			<ol class="carousel-indicators">
				<li data-target="#banner" data-slide-to="0" class="active"></li>
				<li data-target="#banner" data-slide-to="1"></li>
				<li data-target="#banner" data-slide-to="2"></li>
			</ol>

			<!--slides -->
			<div class="carousel-inner" role="listbox">
				
				<?php 
				$Active = TRUE ;
				foreach ($homepage as $home)
				{?>
					<div 
					class="item fadeInLeft fill carousel-item carousel-image 
						<?php if($Active == TRUE)
						{ echo 'active' ; $Active = FALSE ;}?> " 
					style="  background-image: url(<?php echo base_url();?>assets/img/homepage/<?php echo $home['image_path']?>); " >
						<div class=" col-md-4 col-md-offset-7   col-sm-4 col-sm-offset-7 fill center-children banner-text"  >
							<div class="bottom-div hidden-xs animated fadeInDown">
								
								<div class="info-box">
									<div class="text-center">
										<?php echo $home['caption'];?> 
									</div>
									
								</div>
							</div>
						</div>
					</div>
				<?php
				} ?> 
				

				

			</div>

			<!--buttons-->
			<a class="left carousel-control " href="#banner" data-slide="prev">
				<span class="icon-prev"></span>
			</a>
			<a class="right carousel-control" href="#banner" data-slide="next">
				<span class="icon-next"></span>
			</a>
		</header>

        <!-- the what we do section-->
		<div class="page-section-red-home">
			<div class=" container  ">
				<div class="col-md-6 col-md-offset-3 container text-center section-header">
					<h1 class="keywords-underline"> 
						What We Do
					</h1>					
				</div>
			</div>

			<div class="container what-we-do">
			
					<div class="col-md-4 col-sm-6 container what-we-do-item">
						<div class='row'>
							<div class="col-md-2 col-sm-2 col-xs-4 what-we-do-icon center-children" >
								<img class=" img" src="<?php echo base_url();?>assets/img/icons/services/fingerprint_white.png"/>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-8">
								<h3>Biometric Systems</h3>
								<p>Our novel biometric systems are used to provide security and speed to businesses. </p>
							</div>
						</div>
					</div>

				
					<div class="col-md-4 col-sm-6 container what-we-do-item ">
						<div class='row'>
							<div class="col-md-2 col-sm-2 col-xs-4 what-we-do-icon center-children " >								
								<img class=" img" src="<?php echo base_url();?>assets/img/icons/services/development_white.png"/>								
							</div>
							<div class="col-md-10 col-sm-10 col-xs-8">
								<h3>Software Development</h3>
								<p>We develop custom software to meet the business needs of our clients. </p>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-6 container what-we-do-item">
						<div class='row'>
							<div class="col-md-2 col-sm-2 col-xs-4 what-we-do-icon center-children" >
								<img class=" img" src="<?php echo base_url();?>assets/img/icons/services/consultation_white.png"/>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-8">
								<h3>Consultancy</h3>
								<p>Get informed decisions on platforms or technology to boost your business. </p>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-6 container what-we-do-item">
						<div class='row'>
							<div class="col-md-2 col-sm-2 col-xs-4 what-we-do-icon center-children" >
								<img class=" img" src="<?php echo base_url();?>assets/img/icons/services/integration_white.png"/>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-8">
								<h3>System integration</h3>
								<p>Our systems are able to integrate with current systems already in use.0</p>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-6 container what-we-do-item">
						<div class='row'>
							<div class="col-md-2 col-sm-2 col-xs-4 what-we-do-icon center-children" >
								<img class=" img" src="<?php echo base_url();?>assets/img/icons/services/support_white.png"/>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-8">
								<h3>Customer Support</h3>
								<p>We offer 24/7 support to our customer. Their issues are addressed in real time. </p>
							</div>
						</div>
					</div>


					<div class="col-md-4 col-sm-6 container what-we-do-item">
						<div class='row'>
							<div class="col-md-2 col-sm-2 col-xs-4 what-we-do-icon center-children" >
								<img class=" img" src="<?php echo base_url();?>assets/img/icons/services/analysis_white.png"/>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-8">
								<h3>Data analysis</h3>
								<p>We drive business intelligence through data analytics on a large scale.</p>
							</div>
						</div>
					</div>

			</div>
			
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-5  col-xs-offset-4">
						<a href="<?php echo base_url('/index.php/');?>services" class="btn btn-info   ">Read More</a>
					</div>
				</div>
			</div>
		</div>

        <!-- card section-->       
		<div class="page-section">
			<div class="rows ">
				<div class="col-md-6 col-md-offset-3 container text-center">
					<h2 class="keywords-underline"> Introducing Biometric Systems</h2>
					<h4> 
						people use our them because they are:
					</h4>
				</div>
			</div>
			
			<div class="container-fluid">
				<div class="col-sm-4 " >
					<div class="card card-block wow fadeInUp">
						<div class="card-container">
							<h2 class="card-title keywords">Secure</h2>
							<p class="card-text">
								The use of our <strong class="keywords"> biometric </strong> ensures that the data is yours and <strong class="keywords"> yours alone </strong>
							</p>
							<div class="row">
								<div class="col-md-3 col-md-offset-9 col-sm-3 col-sm-offset-9 col-xs-3 col-xs-offset-9">
									<img class="card-stamp" src='<?php echo base_url();?>assets/img/smart_logo.jpg'>
								</div>
							</div>
						</div>
					</div>	
				</div>

				<div class="col-sm-4 " >
					<div class="card card-block wow fadeInUp ">
						<div class="card-container">
							<h2 class="card-title keywords">Fast</h2>
							<p class="card-text">
								Our state of the art systems make usage of our cards as simple as <strong class="keywords"> Tap and Go </strong>
							</p>
							<div class="row">
								<div class="col-md-3 col-md-offset-9 col-sm-3 col-sm-offset-9 col-xs-3 col-xs-offset-9">
									<img class="card-stamp" src='<?php echo base_url();?>assets/img/smart_logo.jpg'>
								</div>
							</div>
						</div>
					</div>	
				</div>

				<div class="col-sm-4 " >
					<div class="card card-block wow fadeInUp">
						<div class="card-container " >
							<h2 class="card-title keywords">Access</h2>
							<p class="card-text">
								Smart support systems are <strong class="keywords"> available 24/7 </strong> to provide care and help to all of our customers
							</p>
						
							<div class="row">
								<div class="col-md-3 col-md-offset-9 col-sm-3 col-sm-offset-9 col-xs-3 col-xs-offset-9">
									<img class="card-stamp" src='<?php echo base_url();?>assets/img/smart_logo.jpg'>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>			
		</div>

		<!-- picture  streak section-->
		<div class="image-div" style="background-image:url('<?php echo base_url();?>assets/img/background_text.jpg'); ">
			<div class=" col-sm-8 col-sm-offset-2 flex-center  pattern-1">
				<ul>
					<li>
						<h1 class="h2-responsive wow fadeIn">
							Everyone deserves reliable access to healthcare
						</h1>
					</li>
					<li>
						<h3 class=" h2-responsive text-xs-center font-italic wow fadeIn" data-wow-delay="0.2s"> 
							our mission is to make this possible 
						</h3>
					</li>
				</ul>
			</div>
		</div>

        <!-- portfolio section-->
		<div class="page-section-red-home">		
			<div class="container  ">
				<div class="col-md-6 col-md-offset-3 container text-center section-header">
					<h1 class="keywords-underline"> 
						Portfolio
					</h1>										
				</div>
				<div class="info-box center-children col-md-8 col-md-offset-2 section-header">
					<p> 							
						We have been at the forefront in providing innovative biometric solution for the management of medical schemes. We have over the years used this experience to build world class systems that meets our client’s needs.
					</p>
				</div>
			</div>
			<div>
				<div class="container">
					<div class="col-sm-3 col-xs-6 wow " id="portfolio">
						<div class=" circle portfolio-circle center-children" id="sportfoliocontainer">							
							<span class="number">12</span>
							<span class="value"> Years </span>							
						</div>
					</div>

					<div class="col-sm-3 col-xs-6 ">
						<div class="circle portfolio-circle center-children" id="sportfoliocontainer">							
							<span class="  number">45</span>
							<span class="value">  clients </span>
						</div>
					</div>

					<div class="col-sm-3 col-xs-6">
						<div class="circle portfolio-circle center-children" id="sportfoliocontainer">						
							<span class="number">1.2</span>
							<span class="value"> Million users </span>						
						</div>
					</div>

					<div class="col-sm-3 col-xs-6">
						<div class="circle portfolio-circle center-children" id="sportfoliocontainer">						
							<span class="number">5</span>
							<span class="text"> Countries </span>						
						</div>
					</div>
					
				</div>
			</div>
		</div>

		<!--clients section-->
		<div class="page-section ">
			<div class=" ">
				<div class="col-md-6 col-md-offset-3 container text-center">
					<h2 class="keywords-underline">Our Clients </h2>
				</div>
			</div>
			
			<div class="container client-logo-container center-children">
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img img-responsive fill" src="<?php echo base_url();?>assets/img/logos/aon.jpg"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/apa.jpg"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/britam.jpg"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/cic.jpg"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/equity.jpg"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/jubilee.jpg"/>
				</div>


				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/kengen.jpg"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/kplc.jpg"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/kra.png"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/madison.png"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/uap.png"/>
				</div>
				<div class="col-md-1 col-sm-2 col-xs-3 ">
					<img class="img  img-responsive fill" src="<?php echo base_url();?>assets/img/logos/resolution.jpg"/>
				</div>

			</div>

		</div>


		
        