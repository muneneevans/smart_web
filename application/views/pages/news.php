<?php?>
<div class="page-section-red cender-children">
    <div class="container">
        <h1 class="row text-center">News</h1>
    </div>
</div>

<div class="page-section-dark">

    <!-- stories section-->
    <div class="container-fluid  ">
        <div class=" col-xs-12 col-sm-3 page-section ">
            <form class="card form news-search-form" action="<?php echo base_url('/index.php/main/searchnews')?>" method="POST">
                <div class="md-form form-group" >
                    <h5 > Search by title</h5>
                    <input class="form-control" type="text" name="SearchText" style="font-weight:50pt;" title="type and press enter"/>
                </div>
                <div class="md-form">
                    <a class="" href="<?php echo base_url('main/news');?>">view all news</a>
                </div>
            </form>
        </div>
        <div class="col-xs-12 col-sm-9 flex-box">
            <?php foreach ($news as $newsitem) { ?>
                <div class=" col-xs-12 col-sm-4 col-md-3 news-item " >
                    <div class="fill news-container " data-toggle="modal" data-target="#myModal"> 
                        <div class=" card fill ">
                            <img class="img img-responsive fill   news-image" src="<?php echo base_url();?>assets/img/news/<?php echo $newsitem['banner_image'] ;?>" />
                            <div class="news-card-content">
                                <a><h5 class="news-title text-center"> 
                                    <?php echo $newsitem['title'] ?>
                                </h5></a>
                            </div>
                            <div class="news-content text-center"><?php echo $newsitem['content']?>
                            </div>
                            
                        </div>
                    </div>  
                </div>
            <?php } ?>
        </div>
    </div>
    
   
    
    <!-- Modal -->
    <div class="modal fade modal-news-content" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <!--Body-->
                <div class="modal-image"></div>
                <div class="modal-body">
                    ...
                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- /.Live preview-->






</div>



