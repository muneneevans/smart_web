<div class="page-section-red cender-children">
	<div class="container">
    	<h1 class="row text-center">Products</h1>
	</div>
</div>
<!-- products section -->
	<div class="page-section ">
		
		<div>
		
			<!--BIMS section-->
			<div class="container page-section">
				<div class="col-md-4 center-children">
					<img class="img img-thumbnail " src="<?php echo base_url(); ?>assets/img/services/bims.JPG"/>
				</div>
				<div class="col-md-8">
					<h2> Biometric Identification Management Solution</h2>
					<p>
						The Smart BIMs can be applied to a wide range of 
						solution areas including financial services customers records 
						in banks and insurance companies, student information 
						management,  licensee information amongst many others. 
						You have accurate customer and employee information 
						management at your fingertips
					</p>
					<p class="text-justify"> 
						To ensure accurate customer identity management, Smart 
						BIMS allows you to:
					</p>

					<li>
						Capture correct customer & employee information 
						authenticated by their biometrics
					</li>
					<li>
						Grant access as required to the correct persons 
						through full proof biometric validation
					</li>
					<li>						
						Give specific licenses and permits, allow safe due 
						remittances to the rightful persons
					</li>
					<li>
						Maintain and manage accurate records of customer 
						information (KYC) and staff information 
					</li>
	
					
				</div>
			</div>

			<!--Time and attendance section-->
			<div class="container page-section-dark ">
				<div class="col-md-4 center-children">
					<img class="img img-thumbnail " src="<?php echo base_url(); ?>assets/img/services/t&a.JPG"/>
				</div>
				<div class="col-md-8">
					<h2> Smart Time & Attendance Solution </h2>
					<p class="text-justify">
						The Smart Time and Attendance solution safeguards an organizations most important resource. This allows you to:
						Smart T&AS gives you:
					</p>
					<li>
						Tracking of staff reporting and exit time
					</li> 
					<li>
						Enhanced Security
					</li>
					<li>
						Ease in report generation for Human Resource function
					</li>
				
				</div>
			</div>

			<!--Smart Canteen section-->
			<div class="container page-section">
				<div class="col-md-4 center-children">
					<img class="img img-thumbnail " src="<?php echo base_url(); ?>assets/img/services/canteen.JPG"/>
				</div>
				<div class="col-md-8">
					<h2> Smart Canteen Management Solution</h2>
					<p class="text-justify">
						Access to meals has never been easier and 
						accurate. Smart CMS allows you to:
					</p>

					<li>
						The solution only grant access to meals through an elaborate biometric validated member registration and authentication process
					</li>
					<li>
						Maintain controls of access to meals and agreed time limit
					</li>
					<li>
						Eliminate duplication and impersonation
					</li>
					<li>
						Reconcile payroll to consumptions per unit/call center
					</li>						 
					</p>
				</div>
			</div>

			<!--Smart Customized Biometric section-->
			<div class="container page-section-dark ">
				<div class="col-md-4 center-children">
					<img class="img img-thumbnail " src="<?php echo base_url(); ?>assets/img/services/solution.JPG"/>
				</div>
				<div class="col-md-8">
					<h2> Smart Customized Biometric & IT solutions</h2>
					<p class="text-justify">
						Smart as an IT Solutions developer 
						and integrator gives you Customized IT 
						Solutions for:-
					</p>
					<li>
						Validated biometrics access to 
						essential services 
					</li>
					<li>
						Secure End to end workflow 
						management systems on essential 
						services
					</li>
					<li>
						Data gathering, reporting and 
						analytics from these essential services
						Let us partner with you to deliver 
						secure, easy and fast access through 
						transformative ICT to your institution.
					</li>
				</div>
			</div>

		</div>
	</div>