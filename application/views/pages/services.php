<div class="page-section-red cender-children">
    <div class="container">
        <h1 class="row text-center">Services</h1>
    </div>
</div>


<div class="page-section container" >
    <div class="service-flex-box  row">
		
		<div class="col-md-4 col-sm-6 service-card-container ">
			<div class=" fill center-children">
				<div class=" card service-card fill">
					<img class="animated " src="<?php echo base_url(); ?>assets/img/icons/services/development.png"/>
					
					<br/>
					<h3 class="keywords">Software Development</h3>
					<span class="icon-bar"></span>
					<div >
						<p>
							Our dedicated team of software developers develop innovative solutions to address challenges faced by businesses on a day to day basis. This is through simple and easy to use applications.
						</p>
					</div>
				</div>
			</div>
		</div>						

		<div class="col-md-4 col-sm-6 service-card-container ">
			<div class="fill center-children">
				<div class=" card service-card fill ">
					<img class="animated " src="<?php echo base_url(); ?>assets/img/icons/services/consultation.png"/>
					
					<br/>
					<h3 class="keywords">Consultancy</h3>
					<span class="icon-bar"></span>
					<div >
						<p>
							We provide expert advice to our clients with regards to the most effective and efficient systems to use in their business.
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4 col-sm-6 service-card-container ">
			<div class=" fill center-children">
				<div class=" card service-card fill ">
					<img class="animated " src="<?php echo base_url(); ?>assets/img/icons/services/integration.png"/>
					
					<br/>
					<h3 class="keywords">System Integration</h3>
					<span class="icon-bar"></span>
					<div >
						<p>
							We strive to ensure that our systems work in perfect harmony with the other applications through use of recommended integration protocols. 
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4 col-sm-6 service-card-container ">
			<div class=" fill center-children">
				<div class=" card service-card fill ">
					<img class="animated " src="<?php echo base_url(); ?>assets/img/icons/services/support.png"/>
					
					<br/>
					<h3 class="keywords">Customer Support</h3>
					<span class="icon-bar"></span>
					<div >
						<p>
							Our dedicated customer service & support desk is accessible on a 24/7/365 basis to address any enquiry. 						
						</p>
						<p class="keywords">
							We are just a phone call away……..!
						</p>
					</div>
					
				</div>
			</div>
		</div>

		<div class="col-md-4 col-sm-6 service-card-container ">
			<div class=" fill center-children">
				<div class=" card service-card fill ">
					<img class="animated " src="<?php echo base_url(); ?>assets/img/icons/services/fingerprint.png"/>
					
					<br/>
					<h3 class="keywords">Biometric Systems</h3>
					<span class="icon-bar"></span>
					<div >
						<p>
							Smart Applications is a leading ICT provider of biometric solutions offering a wide range of solutions that include:
						</p>
						<ul>
							<li>Automated Medical scheme management solution</li>
							<li>Time & Attendance Solution</li>
							<li>Canteen Manage Solution</li>
							<li>Identity Management Solution</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4 col-sm-6 service-card-container ">
			<div class=" fill center-children">
				<div class=" card service-card fill ">
					<img class="animated " src="<?php echo base_url(); ?>assets/img/icons/services/analysis.png"/>
					
					<br/>
					<h3 class="keywords">Data Analysis</h3>
					<span class="icon-bar"></span>
					<div >
						<p>
							We empower businesses to make informed decisions through provision of meaningful interpretations to data. Our cutting edge technology and analytics tools deliver business intelligence.
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4 col-sm-6 service-card-container ">
			<div class=" fill center-children">
				<div class=" card service-card fill ">
					<img class="animated " src="<?php echo base_url(); ?>assets/img/icons/services/smart_card.png"/>
					
					<br/>
					<h3 class="keywords">Smart Cards</h3>
					<span class="icon-bar"></span>
					<div >
						<p>
							Our biometric smart card utilizes certified contactless smart card technology. The cards have an embedded microprocessor chip.
							These comply to the recommended ISO standard 14443B for contactless cards, a higher standard to version ISO7816.
							This capability makes the smart cards versatile for use in multiple applications such as:
						</p>
						<ul>
							<li>Access to medical services</li>
							<li>Access to meals at staff canteen</li>
							<li>Time & Attendance</li>
							<li>4.	Staff cards – The same medical biometric card can be used as a staff card</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

