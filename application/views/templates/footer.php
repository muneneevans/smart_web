                <!--THe footer section-->
		<footer  class=" smart-footer">
			<div class="container">
				<div class="col-md-2 col-sm-3 col-xs-6 text-center">
					<h4>Company</h4>
					<ul>
						<li><a class="footer-link" href="<?php echo base_url('/index.php/');?>main/services">Services</a></li>
						<li><a class="footer-link" href="<?php echo base_url('/index.php/');?>main/products">Products</a></li>
						<li><a class="footer-link" href="<?php echo base_url('/index.php/');?>main/clients">Partners</a></li>
						<li><a class="footer-link" href="<?php echo base_url('/index.php/');?>main/contact">Contact</a></li>
						<li><a class="footer-link" href="<?php echo base_url('/index.php/');?>main/careers">Careers</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-3 col-xs-6 text-center">
					<h4>Resources</h4>
					<ul>
						<li><a class="footer-link" href="#">Provider Manuals</a></li>
						<li><a class="footer-link" href="<?php echo base_url('/index.php/');?>main/faqs">FAQs</a></li>
						<li><a class="footer-link" href="<?php echo base_url();?>resources/pdf/how_smart_works.pdf" target="blank">How Smart Works</a></li>
						<li><a class="footer-link" href="<?php echo base_url('/index.php/');?>main/contact">Contact</a></li>
					</ul>
				</div>

				<div class="col-md-2 col-sm-3 col-xs-5 text-center">
					<h4>media</h4>
					<ul class="text-center">
						<li ><a class="footer-link" href="#">Events</a></li>
						<li ><a class="footer-link" href="#">News</a></li>
										
					</ul>
				</div>

				<div class="col-md-6 col-sm-3 col-xs-12 ">
					<h4>Smart Applications International</h4>
					<ul>
						<li>International House</li>
						<li>P.O. Box: 57776 - 00200, Nairobi, </li>
						<li>kenya</li>
						<li>Tel: 254 20 320 6000 <a href="tel: 254 20 320 6000" > call </a></li> 
						<li>Cel: +254 733 320 600, +254 718 222 000</li>
						<li><a href="mailto:info@smartapplicationsgroup.com" >Email: info@smartapplicationsgroup.com (click to send email)</a></li>
												
					</ul>
				</div>

			</div>
			
		</footer>
                <em>&copy; Copyright 2016 Smart Applications International </em>
        </body>
		
</html>