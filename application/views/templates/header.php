<html>
	<head>
		<title><?php echo $title; ?></title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="Smart Applications">

	
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/favicon.ico" />
		<link rel="stylesheet" type="text/css" media="screen"  href="<?php echo base_url();?>assets/css/bootstrap.min.css" >
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/style.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/mdb.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/mdb.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/bootstrap-theme.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/trumbowyg.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/trumbowyg.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/jquery-ui.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/jquery-ui.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/jquery-ui.theme.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url();?>assets/css/jquery-ui.theme.css">
		

		<script src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
		<script src="<?php echo base_url();?>assets/js/progressbar.js"></script>
		<script src="<?php echo base_url();?>assets/js/smart.js"></script>
		<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/mdb.js"></script>
		<script src="<?php echo base_url();?>assets/js/mdb.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/npm.js"></script>
		<script src="<?php echo base_url();?>assets/js/trumbowyg/trumbowyg.js"></script>
		<script src="<?php echo base_url();?>assets/js/trumbowyg/trumbowyg.min.js"></script>
		
	</head>
	<body>
		<nav class="navbar navbar-fixed-top animated   smart-navbar">
			<div class="">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>                        
					</button>
					<a class="navbar-brand nav-logo-container" href="<?php echo base_url('/index.php/');?>main/home">
						<img class="logo img " src="<?php echo base_url();?>assets/img/smart_logo.jpg"/>
					</a>
				</div>
				<div class="collapse navbar-collapse navbar-right container " id="myNavbar">
					<ul class="nav navbar-nav menu">						

						<!--About-->
						<li class="dropdown">
							<a class="dropdown-toggle menu-item"  data-toggle="dropdown"  href="#">
							About 								
							</a>
							<ul class="dropdown-menu">
								<li><a class="menu-subitem" href="<?php echo base_url('/index.php/');?>main/services">Services</a></li>
								<li><a class="menu-subitem" href="<?php echo base_url('/index.php/');?>main/products">Products</a></li>
								<li><a class="menu-subitem" href="<?php echo base_url('/index.php/');?>main/about">About us</a></li>
								<li><a class="menu-subitem" href="<?php echo base_url('/index.php/');?>main/clients">Clients</a></li>
							</ul>
						</li>

						<!--Media-->
						<li class="dropdown">
							<a class="dropdown-toggle menu-item"  data-toggle="dropdown"  href="#">Media 
								
							</a>
							<ul class="dropdown-menu">
								<li><a class="menu-subitem" href="<?php echo base_url('/index.php/');?>main/news">News</a></li>
								<li><a class="menu-subitem" href="#">Events</a></li>
							</ul>
						</li>

						<!--Help-->
						<li class="dropdown">
							<a class="dropdown-toggle menu-item" data-toggle="dropdown"   href="#">
							 Help								
							</a>
							<ul class="dropdown-menu">
								<li><a class="menu-subitem" href="<?php echo base_url('/index.php/');?>main/news">User Manuals</a></li>
								<li><a class="menu-subitem" href="<?php echo base_url();?>resources/pdf/how_smart_works.pdf" target="blank">How Smart Works</a></li>
								<li><a class="menu-subitem" href="<?php echo base_url('/index.php/');?>main/faqs">FAQ</a></li>
							</ul>
						</li>

						<!--Contact-->
						<li><a class="menu-item" href="<?php echo base_url('/index.php/');?>main/Contact">Contact</a></li>

						<!--Careers-->
						<li><a class="menu-item" href="<?php echo base_url('/index.php/');?>main/careers">Careers</a></li>
						
						
					</ul>
				
				</div>
			</div>
		</nav>
                
