
$(document).ready(
	function()
	{
		$('.carousel').carousel({
             interval: 4000
         })
		 
		wow = new WOW(
			{
				boxClass:     'wow',      // default
				animateClass: 'animated', // default
				offset:       0,          // default
				mobile:       true,       // default
				live:         true,        // default
				callback: function(box)
				{
					var element_id = $(box).attr("id");
   					if(element_id == "portfolio")
					   {
						  FormatNumbers();
					   }
				}
			}
		)
		wow.init();

		$('.howsmartworks-div').scroll(function () {
			switchImage($(this));
		});
			
		wow = new WOW(
			{
				boxClass:     'storya',      // default
				animateClass: 'animated', // default
				offset:       200,          // default
				mobile:       true,       // default
				live:         true,        // default
				callback: function(box)
				{
					alert('flagged') ;
					var element_id = $(box).attr("id");
   					if(element_id == "portfolio")
					   {
						  FormatNumbers();
					   }
				}
			}
		)
		wow.init();
		
		$( ".datepicker" ).datepicker();
		

		//LoadProgressBar();
		SetBannerText();

		EqualHeights();
		//FormatNumbers();

		$(".news-item").click(
			function()
			{	
				ShowNews($(this));
			}
		);


		$(".edit-news-button").click(function(){
			var thatid = $(this).attr('id') ;
			alert(thatid) ;
		});

		SetUpTrumbo();

	} 

);


function switchImage(control) {
	var images = [
			  "http://localhost/smart_web/assets/img/how_smart_works/done.JPG",
			  "http://localhost/smart_web/assets/img/how_smart_works/auth.JPG",			  
			  "http://localhost/smart_web/assets/img/how_smart_works/doctor.JPG",
			  "http://localhost/smart_web/assets/img/how_smart_works/info.JPG",
			  "http://localhost/smart_web/assets/img/how_smart_works/auth.JPG",
			  "http://localhost/smart_web/assets/img/how_smart_works/card.JPG",
			  "http://localhost/smart_web/assets/img/how_smart_works/clinic.JPG",
			  "http://localhost/smart_web/assets/img/how_smart_works/sick.JPG",


			  ];

			var lastScrollTop = 0;
			
			var height = ( $(control)[0].scrollHeight - $(control).height()) ;
			var st = $(control).scrollTop();

			position = Math.floor( (height-st)/height * 9 ) ;
			$('#countr').text( st + ":" + height + " :" +  position);
			
			$(".howsmartworks-img").attr('src',  images[position - 1]  );
	
			
			

    //var sTop = $(control).scrollTop();
    //var index = sTop > 0 ? $(document).height() / sTop : 0;
    //index = Math.round(index) % images.length;
    //console.log(index);
 //   $(".howsmartworks-img").attr('src',  images[index]  );
}

function EqualHeights()
{
		//make news items have same height
		//$(".news-image").height(Math.max.apply(null, $(".news-image").map(function() { return $(this).height(); })));
		//$(".news-border-box").height(Math.max.apply(null, $(".news-border-box").map(function() { return $(this).height(); })));
		//$(".service-card").height(Math.max.apply(null, $(".service-card").map(function() { return $(this).height(); })));
}

function ShowNews(item)
{
	//get the image and text
	var title = $(item).children().find(".news-title").clone()[0];
	var image = $(item).children().find(".news-image").removeClass("").clone().removeClass("news-image").addClass("modal-image")[0];
	var content = $(item).children().find(".news-content").clone().removeClass("news-content")[0];
	var close = $(document).children().find(".close").clone()[0];


	var mtitle = $(".modal-news-content").find(".modal-title").empty().append(title.innerHTML)[0];
	var mcontent = $(".modal-news-content").find(".modal-image").empty().append(image)[0];
	var mcontent = $(".modal-news-content").find(".modal-body").empty().append(content.innerHTML)[0];



}

function FormatNumbers()
{
	$('.number').each(function () 
		{
    		$(this).prop('Counter',0).animate(
    			{
			        Counter: $(this).text()
			    }, {
			        duration: 4000,
			        easing: 'swing',
			        step: function (now) {
			            $(this).text(Math.floor(now));
			        }
			    });
		});
}

function SetBannerText()
{
	var bannertext_list = ['...Anywhere' , '...Anytime' , '...For the whole Family'];
	
	for( var i = 0 ; i < bannertext_list.length; i++)
	{
		//setTimeout(b[i])
	}


	sleep(3000).then(() => {
		$(".bannertext").removeClass("fadeInRight").removeClass("fadeOutLeft");
		$(".bannertext").text("...for the whole family").removeClass("fadeOutLeft").addClass("fadeInRight");
	});	
	
}


function sleep(ms) 
{
  return new Promise(resolve => setTimeout(resolve, ms));
}

function ScrollText()
{
	var downloadButton = document.getElementById("download");
	var counter = 10;
	var newElement = document.createElement("p");
	newElement.innerHTML = "You can download the file in 10 seconds.";
	var id;

	downloadButton.parentNode.replaceChild(newElement, downloadButton);

	id = setInterval(function() {
		counter--;
		if(counter < 0) {
			newElement.parentNode.replaceChild(downloadButton, newElement);
			clearInterval(id);
		} else {
			newElement.innerHTML = "You can download the file in " + counter.toString() + " seconds.";
		}
	}, 1000);

}

function LoadProgressBar()
{
	var bar = new ProgressBar.Circle(portfoliocontainer, {
			color: '#aaa',
			// This has to be the same size as the maximum width to
			// prevent clipping
			strokeWidth: 4,
			trailWidth: 1,
			easing: 'easeInOut',
			duration: 1400,
			text: {
				autoStyleContainer: false
			},
			from: { color: '#aaa', width: 1 },
			to: { color: '#333', width: 4 },
			// Set default step function for all animate calls
			step: function(state, circle) {
				circle.path.setAttribute('stroke', state.color);
				circle.path.setAttribute('stroke-width', state.width);

				var value = Math.round(circle.value() * 1000);
				if (value === 0) {
				circle.setText('');
				} else {
				circle.setText(value);
				}

			}
			});
}

function SetUpTrumbo()
{
	
		$('.trumbo').trumbowyg({
			btns: [
        ['viewHTML'],
        ['formatting'],
        'btnGrp-semantic',
        ['superscript', 'subscript'],
        ['link'],

        'btnGrp-justify',
        'btnGrp-lists',
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
    ]
	});

	$('.trumbo-small').trumbowyg({
			btns: [
        ['viewHTML'],
        ['formatting'],
        'btnGrp-semantic',        
        ['link'],
        'btnGrp-lists',
    ]
	});
}

function showImage(input)
 {
alert('change' ) ;
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#blah').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}


function readURL(input , id) 
{
	if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
					$('#' + id).attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
	}
}