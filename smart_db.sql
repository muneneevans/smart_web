-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2016 at 08:02 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smart_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE IF NOT EXISTS `applicants` (
  `applicant_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sur_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `other_names` text NOT NULL,
  `sex` char(1) NOT NULL DEFAULT 'F',
  `primary_email` varchar(100) NOT NULL,
  `secondary_email` varchar(100) NOT NULL,
  `primary_phone` varchar(100) NOT NULL,
  `secondary_phone` varchar(100) NOT NULL,
  `country` text NOT NULL,
  `resident` text NOT NULL,
  `cv_path` varchar(200) NOT NULL,
  PRIMARY KEY (`applicant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
  `application_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `date_applied` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `client_id` int(11) NOT NULL,
  `client_name` text NOT NULL,
  `logo` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `question_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `tags` text NOT NULL,
  PRIMARY KEY (`question_id`),
  FULLTEXT KEY `answer` (`answer`),
  FULLTEXT KEY `question` (`question`),
  FULLTEXT KEY `tags` (`tags`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`question_id`, `question`, `answer`, `tags`) VALUES
(1, 'What happens if person is too ill to present their finger?', 'The Hospital should notify the Insurance/Organization for off Smart billing.', ''),
(2, 'What do I do if the client’s fingerprint does not match?', 'If the card has been used before, there is a possibility that it is not the right member presenting the fingerprint. Some few cases is because the wrong fingerprint was registered, so try all fingers otherwise contact Smart/Insurance/Organization for further advice.', ''),
(3, 'What do we do if there is a power blackout at the healthcare facility?', 'The Hospital should notify the Insurance/Organization & Smart for off Smart billing authorization.', ''),
(4, 'What should I do if the network is down and the system is offline?', 'When the Smart system goes offline, for safe billing during this process, the provider should contact Smart for confirmation of the member validity/card balances.\r\nSmart system can operate on an OFFFLINE MODE.\r\n', ''),
(5, 'What should I do if the patient disputes/doubts their Smart card balance?', 'The Hospital should contact Smart for balance confirmation. Usually the member balance is affected by other family member utilisation. However it is important to confirm the system is already ONLINE.', ''),
(6, 'How do I get replacement for Smart equipment’s that have been damaged/ lost?', 'The Hospital should inform Smart for further advice in arrangement for equipment replacement. For damage/lost attributed to the facility, the respective facility will take liability.', ''),
(7, 'How will the Insurance know that I have billed their clients on Smart?', 'Please ensure you always print the Smart utility report to accompany submitted invoices.', ''),
(8, 'What do I need to do if I get a patient whose card is physically damaged/not recognizable on Smart?', 'The Hospital should call the Insurance for off Smart billing authorization and advice the patient to return the card to insurance/organization for replacement.', ''),
(9, 'Should I retain Smart cards in case of system/power failure?', 'We do not recommend retention of Smart cards. For any system downtime or inability to capture bill through Smart, please contact respective Insurance/organization for off Smart billing.', ''),
(10, 'What if the trained Smart user is not available to run the system?', 'We recommend that more than one person is trained on use of Smart system.\r\nSmart provides simple user guide in case of absence of trained users. If one is stuck, please contact Smart for quick guidance.\r\nThe Smart team will carry out refresher training at least once in every quarter.\r\n', ''),
(11, 'Whom do I call when Smart system develops a challenge/gives an error on use?', 'For all system related issues or card errors Smart should be the first point of contact. If Smart confirms that the challenge is associated with the Insurance policy of the member, Smart shall advise to contact the respective Insurance or organization.', ''),
(12, 'Where do I get the Smart contact?', 'The Smart contact can be found on the Smart system window and on the face of the Smart card reader.', ''),
(13, 'Do I need to advise Smart in case our IT consultant is working on the network or computer where Smart is installed?', 'It is highly recommended that you inform Smart in case you have plans to make any changes on the computer or network where Smart is installed since this can affect the Smart system, its connectivity and the data.\r\nPlease note that if Smart data is lost, Smart will not guarantee 100% recovery, especially cases where all transaction have not been transmitted.\r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `homepageheader`
--

CREATE TABLE IF NOT EXISTS `homepageheader` (
  `homepage_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(100) NOT NULL,
  `caption` text NOT NULL,
  PRIMARY KEY (`homepage_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `homepageheader`
--

INSERT INTO `homepageheader` (`homepage_id`, `image_path`, `caption`) VALUES
(1, 'family.JPG', '<h1>Welcome to Smart Applications International<br></h1><br><h3>your home for biometric systems<br></h3>'),
(2, 'banner2.PNG', '<h1>smart wins oracle open world 2016 award</h1><br><br><h3>we are proud to be winners of the oracle open world award<br></h3>'),
(3, 'lady.JPG', '<h1>Get quick access to medical care from anywhere using Smart</h1><br><h3><a href="https://www.google.com">read more</a><br></h3>');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `job_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `skills` text NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `title`, `description`, `skills`) VALUES
(1, 'CSS officer', 'asdfasdf', 'degree in IT, good Communication skills, '),
(2, 'Java developer', 'Create systems\r\nDebug systems ', 'Java, web ');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `replied` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `name`, `email`, `message`, `date_sent`, `replied`) VALUES
(1, 'Evans Munene', 'munene.evansk@gmail.com', 'I need some information', '2016-10-25 10:42:06', 'UNREAD'),
(2, 'Evans Munene', 'munene.evansk@gmail.com', 'I need some information', '2016-10-25 10:46:56', 'UNREAD'),
(3, 'Evans Munene', 'munene.evansk@gmail.com', 'I need some information', '2016-10-25 10:47:10', 'UNREAD'),
(4, 'Evans Munene', 'munene.evansk@gmail.com', 'I need some information', '2016-10-25 10:49:44', 'UNREAD'),
(5, 'overmars munene', 'munene.evansk@gmail.com', 'some more questions\r\n', '2016-10-25 10:50:21', 'UNREAD');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `date_published` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `content` text NOT NULL,
  `short_form` text NOT NULL,
  `banner_image` text NOT NULL,
  PRIMARY KEY (`news_id`),
  FULLTEXT KEY `content` (`content`),
  FULLTEXT KEY `title` (`title`),
  FULLTEXT KEY `short_form` (`short_form`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `title`, `date_published`, `date_updated`, `content`, `short_form`, `banner_image`) VALUES
(1, 'Smart wins Oracle Open World award', '2016-10-20 05:56:35', '2016-11-01 16:23:28', '    '' Lorem ipsum dolor sits amet, consectetur adipisicing elit. Pariatur laboriosam nihil exercitationem veniam suscipit sed ad, nulla facilis sapiente assumenda labore magni ullam officia laudantium aperiam, excepturi deleniti nam reprehenderit: em;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur laboriosam nihil exercitationem veniam suscipit sed ad, nulla facilis sapiente assumenda labore magni ullam officia laudantium aperiam, excepturi deleniti nam reprehenderit: em;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur laboriosam nihil exercitationem veniam suscipit sed ad, nulla facilis sapiente assumenda labore magni ullam officia laudantium aperiam, excepturi deleniti nam reprehenderit: em;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur laboriosam nihil exercitationem veniam suscipit sed ad, nulla facilis sapiente assumenda labore magni ullam officia laudantium aperiam, excepturi deleniti nam reprehenderit: em;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur laboriosam nihil exercitationem veniam suscipit sed ad, nulla facilis sapiente assumenda labore magni ullam officia laudantium aperiam, excepturi deleniti nam reprehenderit: em;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur laboriosam nihil exercitationem veniam suscipit sed ad, nulla facilis sapiente assumenda labore magni ullam officia laudantium aperiam, excepturi deleniti nam reprehenderit: em;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur laboriosam nihil exercitationem veniam suscipit sed ad, nulla facilis sapiente assumenda labore magni ullam officia laudantium aperiam, excepturi deleniti nam reprehenderit: em;''', '''Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum illo odit quasi, iste accusantium illum nam aut reiciendis modi rerum, eos ullam. Esse, ab nisi, tenetur accusantium non eveniet minima.''\n''Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum illo odit quasi, iste accusantium illum nam aut reiciendis modi rerum, eos ullam. Esse, ab nisi, tenetur accusantium non eveniet minima.''\n''Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum illo odit quasi, iste accusantium illum nam aut reiciendis modi rerum, eos ullam. Esse, ab nisi, tenetur accusantium non eveniet minima.''\n''Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum illo odit quasi, iste accusantium illum nam aut reiciendis modi rerum, eos ullam. Esse, ab nisi, tenetur accusantium non eveniet minima.''\n', 'oracle_open_world.png'),
(2, 'insurance startup wins Seedstars', '2016-10-23 07:57:03', '2016-11-04 17:16:53', 'Medical insurance platform, InukaPap, was selected as the winner for Seedstars World in Kenya. The pitching competition took place on 7 October.\r\n\r\nInukaPap will represent Kenya in the Seedstars World final taking place in April 2017 in Switzerland. They will compete against 60 other startups from around the world for the chance to win up to US$1-million in equity investment.\r\n\r\nSecond place was awarded to road maintenance startup Mobilized Construction, which uses local labour to build climate-resistant roads. The final place in the top three went to Ujuzi Software Solutions Ltd, a company that AML solutions for all segments.\r\n\r\nThe other contestants consisted of Lakt co, OptiSmart Solutions, Kuza, Ujuzi Software Solutions Ltd, Flexpay Technologies Mobilized Construction, Farmers Pride, Olivine Technology, Student Finance Africa (SFA), and iNuka Pap limited.\r\n\r\n“The Emerging markets and Africa at large provide ample Scaling opportunities and unique clients. Kenyan entrepreneurs should not be left behind in taking advantage of this while at the same time ensuring that they have a solid team to do so,” said local ambassador for Seedstars World, Douglas Ogeto, in a press release to Ventureburn.\r\n\r\nStanbic Bank was the main sponsor of the event, with Omidyar Network, Microsoft, Intel, and Pawa254 providing further support.', 'Medical insurance startup InukaPap wins Seedstars World in Kenya', 'medical_startups.jpg'),
(3, 'Kenya student: How health insurance saved my life', '2016-10-26 10:05:45', '2016-11-04 19:02:41', ' <p> Eighteen-year-old high school student Edger Mulili says he would not be alive today if his father had not contributed $5 (£4) a month to a government health insurance scheme in Kenya.\r\n\r\nMr Mulili has cancer of the oesophagus.\r\n\r\nHe is currently undergoing a five-week chemotherapy and radiotherapy course at Nairobi Hospital, one of the country''s leading private health facilities, located in the capital.\r\n\r\nIn recent weeks, Kenya''s decades'' old National Hospital Insurance Fund (NHIF) has begun offering private treamtment to policyholders suffering from chronic conditions.\r\n''Financial catastrophe''\r\n\r\nMr Mulili is among the first beneficiaries of this new service.\r\n\r\n"If we didn''t have the NHIF cover I would have died and I wouldn''t have blamed my parents for failing to pay for treatment because they just cannot afford.\r\n\r\n"The full course of treatment costs half a million Kenyan shillings ($5,000).\r\n\r\n"Even if we sold everything we could, and asked our family and friends to fund-raise, I don''t think we would have received enough money for treatment."\r\n\r\nThe insurance pays a maximum of $3,500 for treatment of chronic conditions such as chemotherapy, radiotherapy and dialysis.\r\n\r\nThe treatment would have been cheaper within the public health sector, but given the option, he chose not to risk the months-long patient waiting list. \r\nAccording to the World Health Organization (WHO), about 100 million people are pushed into poverty and 150 million globally suffer "financial catastrophe" by spending their money directly on healthcare.\r\n\r\nTo help guard against this, there is now a push for governments to provide universal health insurance schemes.\r\n\r\nA 2012 study carried out by the UN Children''s Fund (Unicef) in Africa and Asia found that only half of the 52 countries surveyed had any form of health insurance scheme, and only four - Ghana, Rwanda, China and Vietnam - were making any visible progress.\r\n\r\nBut as in many developing countries, the vast majority of contributors to Kenya''s health insurance scheme have jobs in the formal sector, such as the civil service or the private sector.\r\n\r\nThat still leaves the greater part of the population, who are either "unemployed, or in subsistence farming and the informal sector" in a vulnerable position, according to the Unicef report. \r\nIn order to reach those people, the NHIF has been out on the road.\r\n\r\nI join the campaign team in Timau, a small town 225km (140 miles) north of Nairobi.\r\nMedia captionAfrican countries have increased spending on healthcare in recent years\r\n\r\nA truck which they have parked in the market is blasting out loud music and a crowd of locals gathers to watch a group of dancers perform.\r\n\r\nEvery so often, the entertainment is paused while one of the staff takes the mic and explains the importance of having health cover.\r\n\r\n"The idea is to educate them so that they can voluntarily enrol," says NHIF chief executive Geoffrey Mwangi.\r\n\r\nBut some think the ambition to attract 12 million people to sign up for the scheme is unrealistic given the $5 a month price tag.\r\n\r\nThe minimum monthly wage in Kenya is about $110.\r\nImage copyright AFP\r\nImage caption Kenya''s public health is stretched\r\n\r\n"The evidence from around the world is that poor or near-poor people don''t tend to buy health insurance unless it is massively subsidised," says Robert Yates, an expert in universal health coverage at London''s Chatham House think-tank.\r\n\r\n"Furthermore, whilst membership remains voluntary, the people who are most likely to join will be high users of healthcare who are likely to consume a greater value of healthcare than their contributions."\r\n\r\nThis can put undue pressure on a scheme''s finances, he adds.\r\n\r\nHe names Rwanda, Burundi, Liberia, Ethiopia, Lesotho and Gabon as examples of African countries where government efforts to bring affordable healthcare to everyone are gaining momentum.\r\n\r\nThe extension of the national health insurance scheme to cover cancer treatment in private hospitals means that patients who need urgent treatment, like Edger Mulili, should now be able to get it.\r\n\r\nTo benefit the general population, much more needs to be done to improve public health systems.\r\n\r\nBut for Mr Mulili, the overwhelming emotion is one of relief.\r\n\r\n"Now I feel like everything will be fine. I feel like I''m going to live," he says, a broad smile spreading across his face.</p><img src="http://www.bing.com/gallery/#images/SkullMural" alt="skull"><br>', 'Edger Mulili''s insurance enabled him to receive urgent private treatment for his cancer', 'student_insurance.jpg'),
(4, 'forth story', '2016-10-26 10:11:37', '2016-10-26 11:25:22', '     ADSASDF', 'there is a new forest', 'banner2.PNG'),
(5, 'fifith story', '2016-10-26 10:12:19', '2016-10-26 10:12:19', 'lkjadsk;laf', 'there is a new forest', 'forest_background.jpg'),
(6, 'Seventh story', '2016-10-26 11:57:22', '2016-11-08 15:18:08', '            <p><a href="https://www.google.com">lorem</a></p>', 'seventh story brief', 'koala.jpg'),
(7, 'Eigth story', '2016-10-28 00:35:25', '2016-11-03 22:29:57', 'what is the essence of these stories ', 'there is a new forest', 'Hydrangeas.jpg'),
(8, 'trumbo test', '2016-11-04 18:05:27', '2016-11-06 18:55:54', '   <br><h1 class="story-body__h1">Kenya student: How health insurance saved my life</h1><p></p><hr><p></p>\r\n\r\n        <figure class="media-landscape has-caption full-width lead">\r\n            <span class="image-and-copyright-container">\r\n                \r\n                </span></figure><figure class="media-landscape has-caption full-width lead"><figcaption class="media-caption">\r\n                <span class="off-screen">Image caption</span>\r\n                <span class="media-caption__text">\r\n                    Edger Mulili''s insurance enabled him to receive urgent private treatment for his cancer\r\n                </span>\r\n            </figcaption>\r\n            \r\n        </figure><p class="story-body__introduction">Eighteen-year-old \r\nhigh school student Edger Mulili says he would not be alive today if his\r\n father had not contributed $5 (£4) a month to a government health \r\ninsurance scheme in Kenya.</p><p>Mr Mulili has cancer of the oesophagus.</p><p>He\r\n is currently undergoing a five-week chemotherapy and radiotherapy \r\ncourse at Nairobi Hospital, one of the country''s leading private health \r\nfacilities, located in the capital. </p><p>In recent weeks, Kenya''s \r\ndecades'' old National Hospital Insurance Fund (NHIF) has begun offering \r\nprivate treamtment to policyholders suffering from chronic conditions.</p><h2 class="story-body__crosshead">''Financial catastrophe''</h2><p>Mr Mulili is among the first beneficiaries of this new service.</p><p>"If\r\n we didn''t have the NHIF cover I would have died and I wouldn''t have \r\nblamed my parents for failing to pay for treatment because they just \r\ncannot afford.</p><p>"The full course of treatment costs half a million Kenyan shillings ($5,000).</p><p>"Even\r\n if we sold everything we could, and asked our family and friends to \r\nfund-raise, I don''t think we would have received enough money for \r\ntreatment."</p><p>The insurance pays a maximum of $3,500 for treatment of chronic conditions such as chemotherapy, radiotherapy and dialysis.</p><p>The\r\n treatment would have been cheaper within the public health sector, but \r\ngiven the option, he chose not to risk the months-long patient waiting \r\nlist. </p><figure class="media-landscape has-caption full-width">\r\n            <span class="image-and-copyright-container">\r\n                \r\n                \r\n                </span></figure><figure class="media-landscape has-caption full-width"><span class="image-and-copyright-container">\r\n                \r\n                \r\n            </span>\r\n            \r\n            <figcaption class="media-caption">\r\n                <span class="off-screen">Image caption</span>\r\n                <span class="media-caption__text">\r\n                    Edger Mulili''s cousin Rita Kiio has been helping him at home preparing meals\r\n                </span>\r\n            </figcaption>\r\n            \r\n        </figure><p>According to the World Health Organization (WHO), \r\nabout 100 million people are pushed into poverty and 150 million \r\nglobally suffer "financial catastrophe" by spending their money directly\r\n on healthcare.</p><p>To help guard against this, there is now a push for governments to provide universal health insurance schemes.</p><p>A\r\n 2012 study carried out by the UN Children''s Fund (Unicef) in Africa and\r\n Asia found that only half of the 52 countries surveyed had any form of \r\nhealth insurance scheme, and only four - Ghana, Rwanda, China and \r\nVietnam - were making any visible progress.</p><p>But as in many \r\ndeveloping countries, the vast majority of contributors to Kenya''s \r\nhealth insurance scheme have jobs in the formal sector, such as the \r\ncivil service or the private sector.</p><p>That still leaves the greater\r\n part of the population, who are either "unemployed, or in subsistence \r\nfarming and the informal sector" in a vulnerable position, according to \r\nthe Unicef report. </p><h2 class="story-body__crosshead">''Too ambitious'' </h2><p>In order to reach those people, the NHIF has been out on the road.</p><p>I join the campaign team in Timau, a small town 225km (140 miles) north of Nairobi. </p><figure class="media-with-caption">\r\n        \r\n            <figure class="media-player" data-playable="{&quot;settings&quot;:{&quot;counterName&quot;:&quot;news.world.africa.story.37584302.page&quot;,&quot;edition&quot;:&quot;International&quot;,&quot;pageType&quot;:&quot;eav2&quot;,&quot;uniqueID&quot;:&quot;37584302&quot;,&quot;ui&quot;:{&quot;locale&quot;:{&quot;lang&quot;:&quot;en-gb&quot;}},&quot;externalEmbedUrl&quot;:&quot;http:\\/\\/www.bbc.com\\/news\\/world-africa-37584302\\/embed&quot;,&quot;insideIframe&quot;:false,&quot;statsObject&quot;:{&quot;clipPID&quot;:&quot;p04blb06&quot;},&quot;playlistObject&quot;:{&quot;title&quot;:&quot;African countries have increased spending on healthcare&quot;,&quot;holdingImageURL&quot;:&quot;http:\\/\\/ichef.bbci.co.uk\\/images\\/ic\\/$recipe\\/p04blmz8.jpg&quot;,&quot;guidance&quot;:&quot;&quot;,&quot;simulcast&quot;:false,&quot;liveRewind&quot;:false,&quot;embedRights&quot;:&quot;blocked&quot;,&quot;items&quot;:[{&quot;vpid&quot;:&quot;p04blb0c&quot;,&quot;live&quot;:false,&quot;duration&quot;:63,&quot;kind&quot;:&quot;programme&quot;}],&quot;summary&quot;:&quot;African countries have increased spending on healthcare&quot;}},&quot;otherSettings&quot;:{&quot;advertisingAllowed&quot;:true,&quot;continuousPlayCfg&quot;:{&quot;enabled&quot;:false},&quot;isAutoplayOnForAudience&quot;:false}}" id="media-player-1" style="background-image: url(&quot;http://ichef.bbci.co.uk/images/ic/608x342/p04blmz8.jpg&quot;); background-repeat: no-repeat; background-position: center center; background-size: contain; background-color: black; overflow: hidden;"></figure>\r\n        \r\n    <figcaption class="media-with-caption__caption"><span class="off-screen">Media caption</span>African countries have increased spending on healthcare in recent years</figcaption>\r\n</figure><p>A truck which they have parked in the market is blasting out\r\n loud music and a crowd of locals gathers to watch a group of dancers \r\nperform. </p><p>Every so often, the entertainment is paused while one of\r\n the staff takes the mic and explains the importance of having health \r\ncover. </p><p>"The idea is to educate them so that they can voluntarily enrol," says NHIF chief executive Geoffrey Mwangi. </p><p>But\r\n some think the ambition to attract 12 million people to sign up for the\r\n scheme is unrealistic given the $5 a month price tag. </p><p>The minimum monthly wage in Kenya is about $110. </p><figure class="media-landscape has-caption full-width">\r\n            <span class="image-and-copyright-container">\r\n                \r\n                \r\n                </span></figure><figure class="media-landscape has-caption full-width"><span class="image-and-copyright-container">\r\n                \r\n                \r\n                 <span class="off-screen">Image copyright</span>\r\n                 <span class="story-image-copyright">AFP</span>\r\n                \r\n            </span>\r\n            \r\n            <figcaption class="media-caption">\r\n                <span class="off-screen">Image caption</span>\r\n                <span class="media-caption__text">\r\n                    Kenya''s public health is stretched\r\n                </span>\r\n            </figcaption>\r\n            \r\n        </figure><p>"The evidence from around the world is that poor or \r\nnear-poor people don''t tend to buy health insurance unless it is \r\nmassively subsidised," says Robert Yates, an expert in universal health \r\ncoverage at London''s Chatham House think-tank. </p><p>"Furthermore, \r\nwhilst membership remains voluntary, the people who are most likely to \r\njoin will be high users of healthcare who are likely to consume a \r\ngreater value of healthcare than their contributions."</p><p>This can put undue pressure on a scheme''s finances, he adds.</p><p>He\r\n names Rwanda, Burundi, Liberia, Ethiopia, Lesotho and Gabon as examples\r\n of African countries where government efforts to bring affordable \r\nhealthcare to everyone are gaining momentum. </p><p>The extension of the\r\n national health insurance scheme to cover cancer treatment in private \r\nhospitals means that patients who need urgent treatment, like Edger \r\nMulili, should now be able to get it.</p><p>To benefit the general population, much more needs to be done to improve public health systems.</p><p>But for Mr Mulili, the overwhelming emotion is one of relief.</p><p>"Now I feel like everything will be fine. I feel like I''m going to live,"  he says, a broad smile spreading across his face.</p><br><br>', 'Kenya student: How health insurance saved my life ', 'Koala1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`,`ip_address`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('1ecf51fc95cca99ad016fbd391596cd9c3acc413', '::1', 1478625448, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383632353433343b),
('23f97ee536dba410fa7e3d20815f7d1d70687629', '::1', 1478466107, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383436363037323b),
('4569c7d92a5a1ee44f14d25b4fd549d4177c0715', '::1', 1478617718, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383631373731383b),
('53386dbec4fc4568e4d891793472a72a73227215', '::1', 1478617988, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383631373732303b757365725f6e616d657c733a31313a226576616e736d756e656e65223b),
('680e0d6e4bc2afbfc6b77bcbee2dd97e293ab7b6', '::1', 1478618871, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383631383834363b757365725f6e616d657c733a31313a226576616e736d756e656e65223b),
('8623746dae351d9af609529df8036643bb69ea45', '::1', 1478618240, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383631383234303b),
('9ad01823a86aad20b53ec26b4ae61065efd67eb6', '::1', 1478466107, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383436363037323b757365725f6e616d657c733a31313a226576616e736d756e656e65223b),
('9cdceeae6509606c779ac0d001fcb6cb8be83fdd', '::1', 1478629398, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383632393339383b),
('aefcf6574680f26aff30004475b7f9109adaafb8', '::1', 1478625358, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383632353330383b),
('ba882e4b7417ba44e7b65ae661c58c0e554c8a3f', '::1', 1478618730, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383631383733303b),
('bd2a3ded12c41de033d71de3421dbbfa16ab3768', '::1', 1478624034, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383632343033343b),
('bdec850313618fee2cac2aa80174383dbbadd327', '::1', 1478618290, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383631383036373b757365725f6e616d657c733a31313a226576616e736d756e656e65223b),
('c6643d6e4fee82f8cb9e50511568c182572879f7', '::1', 1478623056, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383632323934383b),
('c8ead1cf546eb851f938850a2796a578fe43d09a', '::1', 1478624158, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383632343033343b757365725f6e616d657c733a31313a226576616e736d756e656e65223b),
('df75fcf9859070c35c51b21602e13d9ed89dac55', '::1', 1478624725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383632343732353b),
('f2cba0f4c7810c3d18eb674bc630b044c044c17d', '::1', 1478625015, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383632343734393b),
('f7e2078bb091df0735b36178cfeca749db0c7074', '::1', 1478629401, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437383632393339383b);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `email`, `password`) VALUES
(1, 'evansmunene', 'munene.evansk@gmail.com', '95db1e04d0ae5c2b5c4650b7c81f085c2f70801e');

-- --------------------------------------------------------

--
-- Table structure for table `vacancies`
--

CREATE TABLE IF NOT EXISTS `vacancies` (
  `vacancy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `people_needed` int(10) unsigned DEFAULT '0',
  `country` text NOT NULL,
  PRIMARY KEY (`vacancy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `vacancies`
--

INSERT INTO `vacancies` (`vacancy_id`, `job_id`, `people_needed`, `country`) VALUES
(1, 1, 3, 'kenya'),
(2, 1, 3, 'kenya');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
